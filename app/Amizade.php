<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amizade extends Model
{
    protected $fillable = ['id_De','id_Para','status'];
}
