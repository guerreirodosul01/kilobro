<?php

namespace App\Events;

use App\Menssagem;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Support\Facades\Auth;

class NovaMenssagem implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $menssagem;
    public $para;
    public $id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($menssagem,$para,$id)
    {
        $this->menssagem = $menssagem;
        $this->para = $para;
        $this->id = $id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('conversa.'.$this->id);
    }

    public function broadcastWith()
    {
        $time = date('Y-m-d H:i:s');
        return [
            'id_De'=>Auth::user()->id,
            'imagem'=>Auth::user()->imagem,
            'id_Para'=>$this->para,
            'menssagem'=>$this->menssagem,
            'status'=>1,
            'id_Conversa'=>$this->id,
            'created_at' => $time,
            'updated_at' => $time
        ];
    }
}
