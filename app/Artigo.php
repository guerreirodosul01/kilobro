<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artigo extends Model
{
    protected $table = 'artigos';
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->hasMany(likesArtigo::class,'artigo_id');
    }

    public function comments(){
        return $this->hasMany(commentsArtigo::class,'artigo_id');
    }
}
