<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pergunta extends Model
{
    protected $table = 'perguntas';
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->hasMany(likesPergunta::class,'pergunta_id');
    }

    public function comments(){
        return $this->hasMany(commentsPergunta::class,'pergunta_id');
    }
}
