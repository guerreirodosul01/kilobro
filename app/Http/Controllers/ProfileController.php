<?php

namespace App\Http\Controllers;

use App\Amizade;
use App\notificacao;
use App\post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller
{
    /**
     * @param $slug
     */
    public function index($slug) {
        $uid = User::where('slug',$slug)->get('id');
        $uid = $uid[0]['id'];

        $friends1 = DB::table('amizades')->where("status","=",1)
            ->leftJoin("users","users.id",'=','amizades.id_De')
            ->where("id_Para",'=',$uid)
            ->get();

        $friends2 = DB::table('amizades')->where("status","=",1)
            ->leftJoin("users","users.id",'=','amizades.id_Para')
            ->where("id_De",'=',$uid)
            ->get();

        $friends = array_merge_recursive($friends1->toArray(),$friends2->toArray());

        $userData = DB::table('users')
            ->leftJoin('profiles', 'profiles.user_id','=','users.id')
            ->where('slug', $slug)
            ->get();
        return view('profile.index', compact('userData','friends'));
    }
    public function uploadFoto(Request $request){
        $file = $request->file('imagem');
        $filename = $file->getClientOriginalName();
        $path = 'img/';

        $file->move($path,$filename);
        $user_id = Auth::user()->id;

        DB::table('users')->where('id', $user_id)->update(['imagem' => $filename]);
        return back();
    }
    public function updateProfile(Request $request){
        $user_id = Auth::user()->id;
        DB::table('profiles')->where('user_id','=',$user_id)->update($request->except('_token'));
        return back();
    }

    public function addFriend($id){
        Auth::user()->addFriend($id);
        return back();
    }

    public function pedidos(){
        $uid = Auth::user()->id;

        $users  = DB::table('amizades')->rightJoin('users','users.id','=','amizades.id_De')
            ->where('status','=','0')
            ->where('amizades.id_Para','=',$uid)->get();



        return view('profile.pedidos',compact('users'));
    }

    public function acceptFriend($name,$id){
        $uid = Auth::user()->id;
        $checkRequest = Amizade::where('id_De',$id)->where('id_Para',$uid)->first();
        if($checkRequest){
            $updateAmizade = DB::table('amizades')->where('id_Para',$uid)->where("id_De",$id)->update(['status' => 1]);

            $notifcations = new notificacao();
            $notifcations->note = 'Pedido de amizade aceito';
            $notifcations->user_hero = $id; // who is accepting my request
            $notifcations->user_logged = Auth::user()->id; // me
            $notifcations->status = '1'; // unread notifications
            $notifcations->save();
            if ($notifcations) {
                return back()->with('msg', 'Agora você é amigo de ' . $name);
            }
        } else {
            return back()->with('msg', 'Agora você é amigo desse usuario');
        }
    }

    public function recusarFriend($id){
        DB::table('amizades')->where('id_Para',Auth::user()->id)
            ->where("id_De", $id)
            ->delete();
        return back()->with('msg',"Pedido recusado");
    }

    public function notificacoes($id) {
        $uid = Auth::user()->id;
        $notes = DB::table('notificacoes')
            ->leftJoin('users', 'users.id', 'notificacoes.user_logged')
            ->where('notificacoes.id', $id)
            ->where('user_hero', $uid)
            ->orderBy('notificacoes.created_at', 'desc')
            ->get();
        $updateNoti = DB::table('notificacoes')
            ->where('notificacoes.id', $id)
            ->update(['status' => 0]);
        return view('profile.notificacoes', compact('notes'));
    }

    public function desfazerAmizade($id){
        $uid = Auth::user()->id;

        DB::table('amizades')->where('id_De','=',$uid)
            ->where('id_Para','=',$id)
            ->delete();

        DB::table('amizades')->where('id_De','=',$id)
            ->where('id_Para','=',$uid)
            ->delete();

        return back();

    }

    public function setToken(Request $request){
        $email = $request->email;
        $checkEmail = DB::table('users')->where('email','=',$email)->get();
        if (count($checkEmail)==0){
            return redirect("/login");
        }else{

            $to = $email;
            $rand = rand(1,500);
            $token1 = bcrypt($rand);
            $token = str_replace(".", "", $token1);
            $token = str_replace(",", "", $token);
            $token = str_replace("-", "", $token);
            $token = str_replace("/", "", $token);

            $insert_token = DB::table('password_resets')->insert(['email'=>$email,'token'=>$token,'created_at'=>Carbon::now()->toDateTimeString()]);

            $url = "kilobro/getToken/".$token;
            Mail::send('profile.email', compact('url'),function($message) use ($to) {
                $emails = [$to];
                $message->to($emails);
                $message->subject("Password reset Link");
            });
            return redirect("/login");
        }
    }

    public function setPass(Request $request){

        $email = $request->email;
        $token = $request->token;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        if($password==$password_confirmation){
            DB::table('users')->where('email','=',$email)->update(['password' => Hash::make( $password)]);
            return redirect("/login");
        }else{
            return back();
        }
//        $checkEmail = DB::table('users')->where('email','=',$email)->get();
//        if (count($checkEmail)==0){
//            echo "wrong email address";
//        }else{
//
//        }
    }

    public function jobs(){
        $jobs = DB::table('users')
            ->Join('jobs','users.id','=','jobs.company_id')
            ->get();
        return view('profile.jobs', compact('jobs'));
    }

    public function job($id){
        $jobs = DB::table('users')
            ->leftJoin('jobs','users.id','=','jobs.company_id')
            ->where('jobs.id',$id)
            ->get();
        return view('profile.job', compact('jobs'));
    }


    //POSTSSSS


    public function posts(Request $request)
    {

        $idPerfil = \App\User::where('slug','=',$request->slug)->get('id');
        return post::with('user', 'likes', 'comments')
            ->where('user_id','=',$idPerfil[0]['id'])
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function addPost(Request $request)
    {
        $content = $request->texto;

        $createPost = DB::table('post')
            ->insert(['content' => $content, 'user_id' => Auth::user()->id,
                'status' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

        return post::with('user', 'likes', 'comments')
            ->where('user_id','=',Auth::user()->id)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function deletePost($id)
    {
        $var = post::find($id);
        $var->delete();
        return post::with('user', 'likes', 'comments')
            ->where('user_id','=',Auth::user()->id)
            ->orderBy('created_at', 'DESC')->get();

    }

    public function updatePost($id, Request $request)
    {
        $updatePost = DB::table('post')->where('id', $id)->update([
            'content' => $request->updatedContent,
        ]);
        if ($updatePost) {
            return post::with('user', 'likes', 'comments')
                ->where('user_id','=',Auth::user()->id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function likePost(Request $request)
    {
        $pontos= DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$request->id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$request->id)
            ->update(['rankpontos' => $pontos]);

        $idPerfil = \App\User::where('slug','=',$request->slug)->get('id');
        $likePost = DB::table('likes')->insert([
            'post_id' => $request->id,
            'user_id' => Auth::user()->id,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
        // if like successfully then display posts
        if ($likePost) {
            return post::with('user', 'likes', 'comments')
                ->where('user_id','=',$idPerfil[0]['id'])
                ->orderBy('created_at', 'DESC')
                ->get();
        }
    }
    public function deslikePost(Request $request)
    {
        $pontos= DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$request->id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos - 1;
        $pontos2 = DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$request->id)
            ->update(['rankpontos' => $pontos]);

        $idPerfil = \App\User::where('slug','=',$request->slug)->get('id');

        $deslikePost = DB::table('likes')
            ->where('post_id', '=', $request->id)
            ->where('user_id','=', Auth::user()->id)
            ->delete();
        // if like successfully then display posts
        if ($deslikePost) {
            return post::with('user', 'likes', 'comments')
                ->where('user_id','=',$idPerfil[0]['id'])
                ->orderBy('created_at', 'DESC')
                ->get();
        }
    }

    public function addComment(Request $request)
    {
        $comment = $request->comment;
        $id = $request->id;
        $idPerfil = \App\User::where('slug','=',$request->slug)->get('id');
        $createComment = DB::table('comments')
            ->insert(['comment' => $comment, 'user_id' => Auth::user()->id, 'post_id' => $id,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()]);

        if ($createComment) {
            return post::with('user', 'likes', 'comments')
                ->where('user_id','=',$idPerfil[0]['id'])
                ->orderBy('created_at', 'DESC')
                ->get();
        }
    }

    public function saveImg(Request $request)
    {
        $img = $request->get('image');

        // remove extra parts
        $exploded = explode(",", $img);

        // extention
        if (str_contains($exploded[0], 'gif')) {
            $ext = 'gif';
        } else if (str_contains($exploded[0], 'png')) {
            $ext = 'png';
        } else {
            $ext = 'jpg';
        }

        // decode
        $decode = base64_decode($exploded[1]);

        $filename = str_random() . "." . $ext;

        //path of your local folder
        $path = public_path() . "/img/" . $filename;

        //upload image to your path

        if (file_put_contents($path, $decode)) {
            // echo "file uploaded" . $filename;
            $content = $request->texto;
            $createPost = DB::table('post')
                ->insert(['content' => $content, 'user_id' => Auth::user()->id, 'image' => $filename,
                    'status' => 0, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

            if ($createPost) {
                return post::with('user', 'likes', 'comments')
                    ->where('user_id','=',Auth::user()->id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
            }

        }


    }

}
