<?php

namespace App\Http\Controllers;

use App\Artigo;
use App\post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CodigoController extends Controller
{
    public function criar()
    {

        return view('codigo.feed' );
    }



    public function novoCodigo(Request $request)
    {
        $pontos= DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->update(['rankpontos' => $pontos]);

        $servidor = 'http://escola.nextline.com.br/index.php';
        $rand = rand(0, 9999999);
        $nome = 'a'.$rand.$request->extencao;
        // Parametros da requisição
        $content = http_build_query(array(
            'password' => 'alunoescola',
            'nome' => $nome,
            'codigo' => $request->conteudo
        ));

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => "Connection: close\r\n".
                    "Content-type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($content)."\r\n",
                'content' => $content
            )
        ));
        // Realize comunicação com o servidor
        $contents = file_get_contents($servidor, null, $context);
        $resposta = json_decode($contents);  //Parser da resposta Json
        print_r($resposta);

        $titulo = $request->titulo;
        $extencao = 'http://escola.nextline.com.br/pagina/'.$nome;
        $time = date('Y-m-d H:i:s');

        $createPost = DB::table('codigos')
            ->insert(['caminho' => $extencao,'titulo' =>$titulo,  'user_id' => Auth::user()->id,
                'status' => 0, 'created_at' => $time, 'updated_at' => $time]);
        $id = DB::table('codigos')->where('created_at','=',$time)
            ->where('user_id' ,'=', Auth::user()->id)->get("id");

        return redirect("infocodigo/".$id[0]->id);
    }


    public function infocodigo($id)
    {
        $caminho = DB::table('codigos')->where('id','=',$id)
            ->get(["caminho","titulo"]);
        return view('codigo.codigo',compact('caminho') );
    }


}
