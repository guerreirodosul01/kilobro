<?php

namespace App\Http\Controllers;

use App\post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()
    {
        $posts = post::with('user', 'likes', 'comments')
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('user.feed', compact('posts'));
    }



    public function posts()
    {
        return post::with('user', 'likes', 'comments')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function addPost(Request $request)
    {
        $content = $request->texto;

        $createPost = DB::table('post')
            ->insert(['content' => $content, 'user_id' => Auth::user()->id,
                'status' => 0, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

        return post::with('user', 'likes', 'comments')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function deletePost($id)
    {
        $var = post::find($id);
        $var->delete();
        return post::with('user', 'likes', 'comments')->orderBy('created_at', 'DESC')->get();

    }

    public function updatePost($id, Request $request)
    {
        $updatePost = DB::table('post')->where('id', $id)->update([
            'content' => $request->updatedContent,
        ]);
        if ($updatePost) {
            return post::with('user', 'likes', 'comments')
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function likePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $likePost = DB::table('likes')->insert([
            'post_id' => $id,
            'user_id' => Auth::user()->id,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);


        // if like successfully then display posts
        if ($likePost) {
            return post::with('user', 'likes', 'comments')->orderBy('created_at', 'DESC')->get();
        }
    }
    public function deslikePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos - 1;
        $pontos2 = DB::table('profiles')
            ->join('post','post.user_id','=','profiles.user_id')
            ->where('post.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $deslikePost = DB::table('likes')
            ->where('post_id', '=', $id)
            ->where('user_id','=', Auth::user()->id)
            ->delete();

        // if like successfully then display posts
        if ($deslikePost) {
            return post::with('user', 'likes', 'comments')->orderBy('created_at', 'DESC')->get();
        }
    }

    public function addComment(Request $request)
    {
        $comment = $request->comment;
        $id = $request->id;

        $createComment = DB::table('comments')
            ->insert(['comment' => $comment, 'user_id' => Auth::user()->id, 'post_id' => $id,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()]);

        if ($createComment) {
            return post::with('user', 'likes', 'comments')->orderBy('created_at', 'DESC')->get();
            // return all posts same as before
        }
    }

    public function saveImg(Request $request)
    {
        $img = $request->get('image');

        // remove extra parts
        $exploded = explode(",", $img);

        // extention
        if (str_contains($exploded[0], 'gif')) {
            $ext = 'gif';
        } else if (str_contains($exploded[0], 'png')) {
            $ext = 'png';
        } else {
            $ext = 'jpg';
        }

        // decode
        $decode = base64_decode($exploded[1]);

        $filename = str_random() . "." . $ext;

        //path of your local folder
        $path = public_path() . "/img/" . $filename;

        //upload image to your path

        if (file_put_contents($path, $decode)) {
            // echo "file uploaded" . $filename;
            $content = $request->texto;
            $createPost = DB::table('post')
                ->insert(['content' => $content, 'user_id' => Auth::user()->id, 'image' => $filename,
                    'status' => 0, 'created_at' => \Carbon\Carbon::now()->toDateTimeString(), 'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]);

            if ($createPost) {
                return post::with('user', 'likes', 'comments')->orderBy('created_at', 'DESC')->get();
            }

        }


    }
}
