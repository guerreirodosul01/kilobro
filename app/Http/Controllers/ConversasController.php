<?php

namespace App\Http\Controllers;

use App\Events\MenssagemNoti;
use App\Events\NovaMenssagem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConversasController extends Controller
{
    public function getConversas()
    {
//        $allUsers1 = DB::table('users')
//            ->Join('conversas','users.id','=','conversas.id_Um')
//            ->where('conversas.id_Dois', '=', Auth::user()->id)
//            ->get();
//        $allUsers2 = DB::table('users')
//            ->Join('conversas','users.id','=','conversas.id_Dois')
//            ->where('conversas.id_Um', '=', Auth::user()->id)
//            ->get();
//        return array_merge($allUsers1->toArray(),$allUsers2->toArray());

        $allUsers1 = DB::table('users')
            ->Join('amizades','users.id','=','amizades.id_De')
            ->where('amizades.id_Para', '=', Auth::user()->id)
            ->where('status','=',1)
            ->get(['users.id','users.name','users.imagem']);
        $allUsers2 = DB::table('users')
            ->Join('amizades','users.id','=','amizades.id_Para')
            ->where('amizades.id_De', '=', Auth::user()->id)
            ->where('status','=',1)
            ->get(['users.id','users.name','users.imagem']);

        $mens = DB::table('menssagens')
            ->where('status','=',1)
            ->get();
        event(new MenssagemNoti($mens));


        return array_merge($allUsers1->toArray(),$allUsers2->toArray());

    }

    public function getConversasId($id)
    {

        $checkCon = DB::table('conversas')->where('id_Um','=',Auth::user()->id)
            ->where("id_Dois",'=',$id)->get();
        $checkCon2 = DB::table('conversas')->where('id_Dois','=',Auth::user()->id)
            ->where("id_Um",'=',$id)->get();
        if (count($checkCon) != 0){
            $userMsg = DB::table('menssagens')
                ->join('users','users.id','=','menssagens.id_De')
                ->where('menssagens.id_Conversa','=',$checkCon[0]->id)
                ->get(['menssagens.id_De','users.imagem','menssagens.menssagem','menssagens.created_at']);
            $upMsg = DB::table('menssagens')
                ->where('menssagens.id_Conversa','=',$checkCon[0]->id)
                ->where('menssagens.id_Para','=',Auth::user()->id)
                ->update(['status' => 0]);
            $mens = DB::table('menssagens')
                ->where('status','=',1)
                ->get();
            event(new MenssagemNoti($mens));
            $userMsg2 = [
                'id_Conversa'=>$checkCon[0]->id,
                'msgs'=>$userMsg,
            ];
            return $userMsg2;
        }else if(count($checkCon2) != 0){
            $userMsg = DB::table('menssagens')
                ->join('users','users.id','=','menssagens.id_De')
                ->where('menssagens.id_Conversa','=',$checkCon2[0]->id)
                ->get(['menssagens.id_De','users.imagem','menssagens.menssagem','menssagens.created_at']);
            $upMsg = DB::table('menssagens')
                ->where('id_Conversa','=',$checkCon2[0]->id)
                ->where('id_Para','=',Auth::user()->id)
                ->update(['status' => 0]);
            $mens = DB::table('menssagens')
                ->where('status','=',1)
                ->get();
            event(new MenssagemNoti($mens));
            $userMsg2 = [
                'id_Conversa'=>$checkCon2[0]->id,
                'msgs'=>$userMsg
            ];
            return $userMsg2;
        }else{
            $sendM = DB::table('conversas')->insert([
                'id_Um'=>Auth::user()->id,
                'id_Dois'=>$id,
                'status'=>1
            ]);
            $conv = DB::table('conversas')
                ->where('id_Um','=',Auth::user()->id)
                ->where('id_Dois','=',$id)
                ->get();
            $userMsg = [
                'id_Conversa'=>$conv->id,
                'msgs'=>[]
                ];
            return $userMsg;
        }

    }

    public function sendMessage(Request $request)
    {
        $conID = $request->conID;
        $msg = $request->msg;
        $userTo = $request->idAmigo;
        $time = date('Y-m-d H:i:s');


        $sendM = DB::table('menssagens')->insert([
            'id_De'=>Auth::user()->id,
            'id_Para'=>$userTo,
            'menssagem'=>$msg,
            'status'=>1,
            'id_Conversa'=>$conID,
            'created_at' => $time,
            'updated_at' => $time
        ]);
        $mens = DB::table('menssagens')
            ->where('status','=',1)
            ->get();
        event(new MenssagemNoti($mens));
        event(new NovaMenssagem($msg,$userTo,$conID));

        if ($sendM){
            $userMsg = DB::table('menssagens')
                ->join('users','users.id','=','menssagens.id_De')
                ->where('menssagens.id_Conversa','=',$conID)->get(['menssagens.id_De','users.imagem','menssagens.menssagem','menssagens.created_at']);

            return $userMsg;
        }

    }

}
