<?php

namespace App\Http\Controllers;

use App\Pergunta;
use App\post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PerguntaController extends Controller
{
    public function criar()
    {

        return view('pergunta.feed' );
    }

    public function posts()
    {
        return post::with('user', 'likes', 'comments')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function pergunta($id)
    {

        return view("pergunta.pergunta");
    }

    public function infoperunta($id)
    {
        $pergunta = Pergunta::with('user', 'likes', 'comments')
            ->where('id','=',$id)
            ->orderBy('created_at', 'DESC')->get();
        return $pergunta;
    }

    public function addPost(Request $request)
    {
        $pontos= DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->update(['rankpontos' => $pontos]);

        $content = $request->texto;
        $titulo = $request->titulo;
        $time = date('Y-m-d H:i:s');

        $createPost = DB::table('perguntas')
            ->insert(['content' => $content,'titulo' =>$titulo,  'user_id' => Auth::user()->id,
                'status' => 0, 'created_at' => $time, 'updated_at' => $time]);
        $id = DB::table('perguntas')->where('created_at','=',$time)
            ->where('user_id' ,'=', Auth::user()->id)->get("id");
        return $id;
    }

    public function deletePost($id)
    {
        $var = Pergunta::find($id);
        $var->delete();
    }

    public function updatePost($id, Request $request)
    {
        $updatePost = DB::table('perguntas')->where('id', $id)->update([
            'content' => $request->updatedContent,
            'titulo'=>$request->titulo
        ]);
        if ($updatePost) {
            return $pergunta = Pergunta::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function likePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('perguntas','perguntas.user_id','=','profiles.user_id')
            ->where('perguntas.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->join('perguntas','perguntas.user_id','=','profiles.user_id')
            ->where('perguntas.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $likePost = DB::table('likesPergunta')->insert([
            'pergunta_id' => $id,
            'user_id' => Auth::user()->id,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
        // if like successfully then display posts
        if ($likePost) {
            return $pergunta = Pergunta::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }
    public function deslikePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('perguntas','perguntas.user_id','=','profiles.user_id')
            ->where('perguntas.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos - 1;
        $pontos2 = DB::table('profiles')
            ->join('perguntas','perguntas.user_id','=','profiles.user_id')
            ->where('perguntas.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $deslikePost = DB::table('likesPergunta')
            ->where('pergunta_id', '=', $id)
            ->where('user_id','=', Auth::user()->id)
            ->delete();

        // if like successfully then display posts
        if ($deslikePost) {
            return Pergunta::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function addComment(Request $request)
    {
        $comment = $request->comment;
        $id = $request->id;

        $createComment = DB::table('commentsPergunta')
            ->insert(['comment' => $comment,'certa' => 0 ,  'user_id' => Auth::user()->id, 'pergunta_id' => $id,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()]);

        if ($createComment) {
            return $pergunta = Pergunta::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function saveImg(Request $request)
    {
        $img = $request->get('image');

        // remove extra parts
        $exploded = explode(",", $img);

        // extention
        if (str_contains($exploded[0], 'gif')) {
            $ext = 'gif';
        } else if (str_contains($exploded[0], 'png')) {
            $ext = 'png';
        } else {
            $ext = 'jpg';
        }

        // decode
        $decode = base64_decode($exploded[1]);

        $filename = str_random() . "." . $ext;

        //path of your local folder
        $path = public_path() . "/img/" . $filename;

        //upload image to your path

        if (file_put_contents($path, $decode)) {
            // echo "file uploaded" . $filename;
            $content = $request->texto;
            $titulo = $request->titulo;
            $time = \Carbon\Carbon::now()->toDateTimeString();
            $createPost = DB::table('perguntas')
                ->insert(['content' => $content,'titulo'=>$titulo, 'user_id' => Auth::user()->id, 'image' => $filename,
                    'status' => 0, 'created_at' => $time, 'updated_at' => $time]);

            $id = DB::table('perguntas')->where('created_at','=',$time)
                ->where('user_id' ,'=', Auth::user()->id)->get("id");
            if ($createPost) {
                return $id;            }

        }


    }
}
