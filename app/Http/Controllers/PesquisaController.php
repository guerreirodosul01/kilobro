<?php

namespace App\Http\Controllers;

use App\Artigo;
use App\Codigo;
use App\Pergunta;
use App\post;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PesquisaController extends Controller
{
    public function pesquisa(Request $pesquisa)
    {
        $perguntas = Pergunta::with('user', 'likes', 'comments')
            ->where('content', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orWhere('titulo', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orderBy('updated_at', 'DESC')->get();

        $artigos = Artigo::with('user', 'likes', 'comments')
            ->where('content', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orWhere('titulo', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orderBy('updated_at', 'DESC')->get();

        $codigos = Codigo::with('user')
            ->where('titulo', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orderBy('updated_at', 'DESC')->get();
        $users =  DB::table('profiles')
            ->leftJoin('users','users.id','=','profiles.user_id')
            ->where('name', 'like', '%' . $pesquisa->pesquisa . '%')
            ->get();
        $jobs =  DB::table('jobs')
            ->leftJoin('users','users.id','=','jobs.company_id')
            ->where('job_title', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orWhere('requirements', 'like', '%' . $pesquisa->pesquisa . '%')
            ->orderByDesc('created_at')
            ->get(['users.slug','users.imagem','jobs.job_title','jobs.created_at','jobs.requirements','jobs.skills',
                'jobs.email']);


        return view("pesquisa.feed", compact('perguntas','codigos','artigos','users','jobs'));
    }
    public function rankbro()
    {

        $users =  User::with('profile')
            ->orderBy('created_at', 'asc')
            ->limit(10)
            ->get();


        return view("pesquisa.rankbro", compact('users'));
    }
}
