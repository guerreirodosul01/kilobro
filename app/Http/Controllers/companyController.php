<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class companyController extends Controller
{
    public function index()
    {
        return view('company.index');
    }

    public function addJobSubmit(Request $request){
        if($request->skills) {
            $skills = implode($request->skills, ',');
        }else {
            $skills = " ";
        }
        $contact_email = $request->email;
        $job_title = $request->job_title;
        $requirements = $request->requirements;
        $com_id = Auth::user()->id;
        $add_job = DB::table('jobs')->insert([
            'skills' => $skills,
            'email' => $contact_email,
            'job_title' => $job_title,
            'requirements' => $requirements,
            'company_id' => $com_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        if($add_job){

            return redirect('/');
        }

    }

    public function viewJobs(){
        $jobs = DB::table('jobs')->where('company_id', Auth::user()->id)
            ->get();
        return view('company.jobs', compact('jobs'));
    }
}
