<?php

namespace App\Http\Controllers;

use App\Artigo;
use App\Pergunta;
use App\post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ArtigoController extends Controller
{
    public function criar()
    {

        return view('artigo.feed' );
    }

    public function posts()
    {
        return post::with('user', 'likes', 'comments')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function novoArtigo(Request $request)
    {
        $pontos= DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->where('profiles.user_id','=',Auth::user()->id)
            ->update(['rankpontos' => $pontos]);

        $content = $request->conteudo;
        $titulo = $request->titulo;
        $time = date('Y-m-d H:i:s');

        $createPost = DB::table('artigos')
            ->insert(['content' => $content,'titulo' =>$titulo,  'user_id' => Auth::user()->id,
                'status' => 0, 'created_at' => $time, 'updated_at' => $time]);
        $id = DB::table('artigos')->where('created_at','=',$time)
            ->where('user_id' ,'=', Auth::user()->id)->get("id");

        return redirect("artigo/".$id[0]->id);
    }
    public function pergunta($id)
    {
        return view("artigo.artigo");
    }

    public function infoartigo($id)
    {
        $artigo = Artigo::with('user', 'likes', 'comments')
            ->where('id','=',$id)
            ->orderBy('created_at', 'DESC')->get();
        return $artigo;
    }


    public function deletePost($id)
    {
        $var = Artigo::find($id);
        $var->delete();
    }


    public function likePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('artigos','artigos.user_id','=','profiles.user_id')
            ->where('artigos.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos + 1;
        $pontos2 = DB::table('profiles')
            ->join('artigos','artigos.user_id','=','profiles.user_id')
            ->where('artigos.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $likePost = DB::table('likesArtigo')->insert([
            'artigo_id' => $id,
            'user_id' => Auth::user()->id,
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
        // if like successfully then display posts
        if ($likePost) {
            return $pergunta = Artigo::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }
    public function deslikePost($id)
    {
        $pontos= DB::table('profiles')
            ->join('artigos','artigos.user_id','=','profiles.user_id')
            ->where('artigos.id','=',$id)
            ->first(['rankpontos']);
        $pontos = $pontos->rankpontos - 1;
        $pontos2 = DB::table('profiles')
            ->join('artigos','artigos.user_id','=','profiles.user_id')
            ->where('artigos.id','=',$id)
            ->update(['rankpontos' => $pontos]);

        $deslikePost = DB::table('likesArtigo')
            ->where('artigo_id', '=', $id)
            ->where('user_id','=', Auth::user()->id)
            ->delete();

        // if like successfully then display posts
        if ($deslikePost) {
            return Artigo::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

    public function addComment(Request $request)
    {
        $comment = $request->comment;
        $id = $request->id;

        $createComment = DB::table('commentsArtigo')
            ->insert(['comment' => $comment ,  'user_id' => Auth::user()->id, 'artigo_id' => $id,
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()]);

        if ($createComment) {
            return $artigo = Artigo::with('user', 'likes', 'comments')
                ->where('id','=',$id)
                ->orderBy('created_at', 'DESC')->get();
        }
    }

}
