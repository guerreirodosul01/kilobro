<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menssagem extends Model
{
    protected $table = 'menssagens';

    public function user() {
        return $this->hasOne('App\User');
    }
    public function conversa() {
        return $this->hasOne('App\Conversa');
    }
}
