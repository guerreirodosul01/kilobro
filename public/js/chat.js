/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const menssagens = new Vue({
    el: '#menssagens',
    data: {
        msg: 'Seus amigos:',
        content: '',
        privsteMsgs:[],
        singleMsgs:[],
        msgFrom: '',
        conID:0,
        idAmigo:'',
        bUrl: window.location.href,

    },

    ready: function(){
        this.created();
    },


    created(){
        axios.get('/getConversas')
            .then(response => {
                this.privsteMsgs = response.data; //we are putting data into our posts array
                Vue.filter('myOwnTime', function(value){
                    moment.updateLocale('pt-br', {
                        months : 'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
                        monthsShort : 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_'),
                        weekdays : 'domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado'.split('_'),
                        weekdaysShort : 'dom_seg_ter_qua_qui_sex_sáb'.split('_'),
                        weekdaysMin : 'dom_2ª_3ª_4ª_5ª_6ª_sáb'.split('_'),
                        longDateFormat : {
                            LT : 'HH:mm',
                            L : 'DD/MM/YYYY',
                            LL : 'D [de] MMMM [de] YYYY',
                            LLL : 'D [de] MMMM [de] YYYY [às] LT',
                            LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                        },
                        calendar : {
                            sameDay: '[Hoje às] LT',
                            nextDay: '[Amanhã às] LT',
                            nextWeek: 'dddd [às] LT',
                            lastDay: '[Ontem às] LT',
                            lastWeek: function () {
                                return (this.day() === 0 || this.day() === 6) ?
                                    '[Último] dddd [às] LT' : // Saturday + Sunday
                                    '[Última] dddd [às] LT'; // Monday - Friday
                            },
                            sameElse: 'L'
                        },
                        relativeTime : {
                            future : 'em %s',
                            past : '%s atrás',
                            s : 'segundos',
                            m : 'um minuto',
                            mm : '%d minutos',
                            h : 'uma hora',
                            hh : '%d horas',
                            d : 'um dia',
                            dd : '%d dias',
                            M : 'um mês',
                            MM : '%d meses',
                            y : 'um ano',
                            yy : '%d anos'
                        },
                        ordinal : '%dº'
                    });
                    return moment(value).fromNow();
                });
            })
            .catch(function (error) {
                console.log(error); // run if we have error
            });
    },

    methods: {
        messages: function(id){
            axios.get('/getConversas/' + id)
                .then(response => {
                    this.singleMsgs = response.data.msgs; //we are putting data into our posts array
                    this.conID = response.data.id_Conversa;
                    this.idAmigo = id;


                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });
        },
        inputHandler(e){
            if(e.keyCode === 13 && !e.shiftKey){
                e.preventDefault();
                this.sendMsg();
            }
        },
        sendMsg(){
            if(this.msgFrom){
                axios.post('/sendMessage', {
                    conID: this.conID,
                    msg:this.msgFrom,
                    idAmigo:this.idAmigo,
                })
                    .then((response) => {

                        if (response.status === 200) {
                            this.singleMsgs = response.data;
                            this.msgFrom ="";
                        }
                    })
                    .catch(function (error) {
                        console.log(error); // run if we have error
                    });
            }
        }
    }
});
