/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/profile.js":
/*!*********************************!*\
  !*** ./resources/js/profile.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
var noti = new Vue({
  el: '.menssagens',
  data: {
    mensnoti: 0,
    idUser: 0,
    bUrl: window.location.href
  },
  ready: function ready() {
    this.created();
  },
  created: function created() {
    this.idUser = $('#idUser').val();
    console.log(this.idUser);
    this.listen();
  },
  methods: {
    listen: function listen() {
      var _this = this;

      Echo.channel('mensnoti').listen("MenssagemNoti", function (numnoti) {
        var num = 0;

        for (var i = 0; i < numnoti['menssagens'].length; i++) {
          if (numnoti['menssagens'][i]['id_Para'] == _this.idUser) {
            num++;
          }
        }

        _this.mensnoti = num;
      });
    }
  }
});
var menssagens = new Vue({
  el: '#menssagens',
  data: {
    msg: 'Seus amigos:',
    content: '',
    privsteMsgs: [],
    singleMsgs: [],
    msgFrom: '',
    conID: 0,
    idAmigo: '',
    bUrl: window.location.href
  },
  ready: function ready() {
    this.created();
  },
  created: function created() {
    var _this2 = this;

    axios.get('/getConversas').then(function (response) {
      _this2.privsteMsgs = response.data; //we are putting data into our posts array

      Vue.filter('myOwnTime', function (value) {
        moment.updateLocale('pt-br', {
          months: 'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
          monthsShort: 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_'),
          weekdays: 'domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado'.split('_'),
          weekdaysShort: 'dom_seg_ter_qua_qui_sex_sáb'.split('_'),
          weekdaysMin: 'dom_2ª_3ª_4ª_5ª_6ª_sáb'.split('_'),
          longDateFormat: {
            LT: 'HH:mm',
            L: 'DD/MM/YYYY',
            LL: 'D [de] MMMM [de] YYYY',
            LLL: 'D [de] MMMM [de] YYYY [às] LT',
            LLLL: 'dddd, D [de] MMMM [de] YYYY [às] LT'
          },
          calendar: {
            sameDay: '[Hoje às] LT',
            nextDay: '[Amanhã às] LT',
            nextWeek: 'dddd [às] LT',
            lastDay: '[Ontem às] LT',
            lastWeek: function lastWeek() {
              return this.day() === 0 || this.day() === 6 ? '[Último] dddd [às] LT' : // Saturday + Sunday
              '[Última] dddd [às] LT'; // Monday - Friday
            },
            sameElse: 'L'
          },
          relativeTime: {
            future: 'em %s',
            past: '%s atrás',
            s: 'segundos',
            m: 'um minuto',
            mm: '%d minutos',
            h: 'uma hora',
            hh: '%d horas',
            d: 'um dia',
            dd: '%d dias',
            M: 'um mês',
            MM: '%d meses',
            y: 'um ano',
            yy: '%d anos'
          },
          ordinal: '%dº'
        });
        return moment(value).fromNow();
      });
    })["catch"](function (error) {
      console.log(error); // run if we have error
    });
  },
  methods: {
    messages: function messages(id) {
      var _this3 = this;

      axios.get('/getConversas/' + id).then(function (response) {
        _this3.singleMsgs = response.data.msgs; //we are putting data into our posts array

        _this3.conID = response.data.id_Conversa;
        _this3.idAmigo = id;

        _this3.listen();

        $('#mens').animate({
          scrollTop: _this3.singleMsgs.length * 10000000
        }, 500);
      })["catch"](function (error) {
        console.log(error); // run if we have error
      });
    },
    inputHandler: function inputHandler(e) {
      if (e.keyCode === 13 && !e.shiftKey) {
        e.preventDefault();
        this.sendMsg();
      }
    },
    sendMsg: function sendMsg() {
      var _this4 = this;

      if (this.msgFrom) {
        axios.post('/sendMessage', {
          conID: this.conID,
          msg: this.msgFrom,
          idAmigo: this.idAmigo
        }).then(function (response) {
          if (response.status === 200) {
            _this4.singleMsgs = response.data;
            _this4.msgFrom = "";
          }

          $('#mens').animate({
            scrollTop: $('#tammens').height()
          }, 500);
        })["catch"](function (error) {
          console.log(error); // run if we have error
        });
      }
    },
    listen: function listen() {
      var _this5 = this;

      Echo.channel('conversa.' + this.conID).listen("NovaMenssagem", function (menssagem) {
        _this5.singleMsgs.push(menssagem);

        $('#mens').animate({
          scrollTop: $('#tammens').height()
        }, 500);
      });
    }
  }
});

/***/ }),

/***/ 1:
/*!***************************************!*\
  !*** multi ./resources/js/profile.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\Kilobro\resources\js\profile.js */"./resources/js/profile.js");


/***/ })

/******/ });