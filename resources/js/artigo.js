/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        titulo: '',
        content: '',updatedContent:'',
        posts: [],
        postId: '',
        successMsg: '',
        commentData:{},
        commentSeen: false,
        image:'',
        bUrl: window.location.href + "/../..",
        qry:'',
        results:[],
        id: 0
    },

    ready: function(){
        this.created();
    },


    created(){
        var rota = window.location.href;
        var resultado = rota.split("/");
        this.id = resultado[resultado.length - 1];
        axios.get(this.bUrl +'/artigo/infoartigo/'+this.id )
            .then(response => {
                var listlike = [];
                for(var tipoNome in response.data) {
                    response.data[tipoNome]['listlikes'] = [];
                    for(var like in response.data[tipoNome]['likes']) {
                        response.data[tipoNome]['listlikes'].push(response.data[tipoNome]['likes'][like]['user_id']);

                    }
                }

                this.posts = response.data; //we are putting data into our posts array


                Vue.filter('myOwnTime', function(value){
                    moment.updateLocale('pt-br', {
                        months : 'janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro'.split('_'),
                        monthsShort : 'jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez'.split('_'),
                        weekdays : 'domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado'.split('_'),
                        weekdaysShort : 'dom_seg_ter_qua_qui_sex_sáb'.split('_'),
                        weekdaysMin : 'dom_2ª_3ª_4ª_5ª_6ª_sáb'.split('_'),
                        longDateFormat : {
                            LT : 'HH:mm',
                            L : 'DD/MM/YYYY',
                            LL : 'D [de] MMMM [de] YYYY',
                            LLL : 'D [de] MMMM [de] YYYY [às] LT',
                            LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                        },
                        calendar : {
                            sameDay: '[Hoje às] LT',
                            nextDay: '[Amanhã às] LT',
                            nextWeek: 'dddd [às] LT',
                            lastDay: '[Ontem às] LT',
                            lastWeek: function () {
                                return (this.day() === 0 || this.day() === 6) ?
                                    '[Último] dddd [às] LT' : // Saturday + Sunday
                                    '[Última] dddd [às] LT'; // Monday - Friday
                            },
                            sameElse: 'L'
                        },
                        relativeTime : {
                            future : 'em %s',
                            past : '%s atrás',
                            s : 'segundos',
                            m : 'um minuto',
                            mm : '%d minutos',
                            h : 'uma hora',
                            hh : '%d horas',
                            d : 'um dia',
                            dd : '%d dias',
                            M : 'um mês',
                            MM : '%d meses',
                            y : 'um ano',
                            yy : '%d anos'
                        },
                        ordinal : '%dº'
                    });
                    return moment(value).fromNow();
                });
            })
            .catch(function (error) {
                console.log(error); // run if we have error
            });
    },

    methods: {
        deletePost(id){
            //alert(id);
            axios.get(this.bUrl +'/artigo/deletePost/' + id)
                .then(response => {
                    window.location.href = "/"
                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });

        },
        likePost(id){
            axios.get(this.bUrl +'/artigo/likePost/'+id)
                .then(response => {
                    var listlike = [];
                    for(var tipoNome in response.data) {
                        response.data[tipoNome]['listlikes'] = [];
                        for(var like in response.data[tipoNome]['likes']) {
                            response.data[tipoNome]['listlikes'].push(response.data[tipoNome]['likes'][like]['user_id']);

                        }
                    }
                    this.posts = response.data; //we are putting data into our posts array
                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });
        },
        deslikePost(id){
            axios.get(this.bUrl +'/artigo/deslikePost/' + id)
                .then(response => {
                    var listlike = [];
                    for(var tipoNome in response.data) {
                        response.data[tipoNome]['listlikes'] = [];
                        for(var like in response.data[tipoNome]['likes']) {
                            response.data[tipoNome]['listlikes'].push(response.data[tipoNome]['likes'][like]['user_id']);

                        }
                    }
                    this.posts = response.data; //we are putting data into our posts array
                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });
        },
        addComment(post,key){

            axios.post(this.bUrl +'/artigo/addComment', {
                comment: this.commentData[key],
                id: post.id,
            })
                .then(function (response) {
                    console.log('saved successfully'); // show if success
                    if(response.status===200){
                        var listlike = [];
                        for(var tipoNome in response.data) {
                            response.data[tipoNome]['listlikes'] = [];
                            for(var like in response.data[tipoNome]['likes']) {
                                response.data[tipoNome]['listlikes'].push(response.data[tipoNome]['likes'][like]['user_id']);

                            }
                        }
                        app.posts = response.data;
                    }
                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });

        },
    }
});
