/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data: {
        titulo: '',
        content: '',
        updatedContent:'',
        posts: [],
        postId: '',
        successMsg: '',
        commentData:{},
        commentSeen: false,
        image:'',
        bUrl: window.location.href + "/../..",
        qry:'',
        results:[],
        slug: ''
    },

    ready: function(){
        this.created();
    },


    created(){

    },

    methods: {
        addPost() {
            axios.post(this.bUrl + '/pergunta/addPost', {
                titulo: this.titulo,
                texto: this.content
            }).then(response => {
                window.location.href = this.bUrl + '/pergunta/' + response.data[0]['id'];
            })
        },

        onFileChange(e){
            var files = e.target.files || e.dataTransfer.files;
            this.createImg(files[0]); // files the image/ file value to our function

        },
        createImg(file){
            // we will preview our image before upload
            var image = new Image;
            var reader = new FileReader;

            reader.onload = (e) =>{
                this.image = e.target.result;
            };
            reader.readAsDataURL(file);
        },

        uploadImg(){
            axios.post(this.bUrl +'/pergunta/saveImg', {
                titulo: this.titulo,
                image: this.image,
                texto: this.content
            })
                .then( (response) =>{
                    window.location.href = this.bUrl + '/pergunta/' + response.data[0]['id'];
                })
                .catch(function (error) {
                    console.log(error); // run if we have error
                });
        },
        removeImg(){
            this.image=""
        },
    }
});
