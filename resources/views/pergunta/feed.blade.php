@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        @if(Auth::check())

                            <div class="card bg-white col-md-9  m-auto">
                                <div v-if="!image">
                                    <form method="post" enctype="multipart/form-data" v-on:submit.prevent="addPost">
                                        <div class="post-new-contain row card-block">


                                            <div class="col-md-12 col-xs-12">
                                                <br>
                                                <h5>Titulo Da Pergunta:</h5>
                                                <br>
                                                <input required v-model="titulo" class="form-control"
                                                       placeholder="O Titulo">
                                                <br>
                                                <h5>Conteudo da Pergunta:</h5>
                                                <br>
                                                <textarea v-model="content" id="post-message" class="form-control "
                                                          rows="3" cols="10"
                                                          placeholder="O que você está pensando ?"
                                                          required=""></textarea>

                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">
                <span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title=""
                      data-original-title="Adicionar uma imagem">
                                          <label for="file-input" class="file-upload-lbl">
                                            <i class="icofont icofont-image text-muted"></i>
                                          </label>

                                          <input @change="onFileChange" id="file-input" type="file"
                                                 accept="image/x-png,image/gif,image/jpeg">
                                        </span>
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Perguntar">
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>

                                <div v-else>
                                    <form method="post" enctype="multipart/form-data" v-on:submit.prevent="uploadImg">
                                        <div class="post-new-contain row card-block">


                                            <div class="col-md-12 col-xs-12">
                                                <br>
                                                <h5>Titulo Da Pergunta:</h5>
                                                <br>
                                                <input required v-model="titulo" class="form-control"
                                                       placeholder="O Titulo">
                                                <br>
                                                <h5>Conteudo da Pergunta:</h5>
                                                <br>
                                                <textarea v-model="content" id="post-message" class="form-control "
                                                          rows="3" cols="10"
                                                          placeholder="O que você está pensando ?"
                                                          required=""></textarea>

                                            </div>
                                            <div class="col-md-12 col-xs-9">
                                                <img :src="image"
                                                     style="display:block;width:80%; margin:auto;margin-top: 20px;margin-bottom: 20px"/><br>
                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">

                                            <label for="file-input" class="file-upload-lbl" @click="removeImg">
                                                <i class="icofont icofont-close text-muted"></i>
                                            </label>


                                            </span>
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Perguntar">
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>
                            </div>

                        @endif
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/pergunta.js')}}" type="text/javascript"></script>
@endsection
