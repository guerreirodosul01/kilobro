@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" >
                    <div>
                        @if(Auth::check())

                            <div class="card bg-white col-md-9  m-auto">
                                <div>
                                    <form method="post"  action="addJobSubmit">
                                        @csrf
                                        <div class="post-new-contain row card-block">


                                            <div class="col-md-12 col-xs-12">
                                                <br>
                                                <h4>Titulo Do Trabalho:</h4>
                                                <br>
                                                <input required type="text" id="job_title" name="job_title" class="form-control" placeholder="O Titulo">

                                                <br>
                                                <h4 id="job_head">Habilidade Requisitadas</h4>
                                                <div class="row">

                                                    <li style="margin:15px; float:left; list-style:none">
                                                        <input class="form-control" type="checkbox" name="skills[]" value="HTML" > HTML
                                                    </li>
                                                    <li style="margin:15px; float:left; list-style:none">
                                                        <input class="form-control" type="checkbox" name="skills[]" value="CSS" > CSS
                                                    </li>
                                                    <li style="margin:15px; float:left; list-style:none">
                                                        <input class="form-control" type="checkbox" name="skills[]" value="PHP" > PHP
                                                    </li>
                                                </div>
                                                <br>
                                                <h5>Requisitos:</h5>
                                                <br>
                                                <textarea placeholder="Os Requisitos" required rows="3" cols="40" name="requirements" class="form-control"></textarea>
                                                <br>
                                                <h4 id="job_head">Email de Contato</h4>
                                                <input placeholder="O Email" required type="email" name="email" class="form-control"><br/>

                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">

                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Criar Trabalho">
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                        <br>
                                        <br>
                                    </form>
                                </div>

                            </div>

                        @endif
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/feed.js')}}" type="text/javascript"></script>
@endsection
