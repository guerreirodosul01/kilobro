@extends('company.master')
<style>
  #job_head{
  color:rgb(6, 99, 52); font-weight:bold;
  }
</style>
@section('content')
<div class="content">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-md-6">
                    <div class="card">
                         <div class="header">
                             <h4 class="title">Add new Job</h4>
                         </div>
                         <div class="content">


                              <form method="post" action="addJobSubmit">
                                  @csrf
                                  <h4 id="job_head">Job Title</h4>
                                      <input type="text" id="job_title" name="job_title" class="form-control">

                                  <h4 id="job_head">Skills</h4>
                                  <div class="row">
                                      <?php // use proper css styling ?>

                                      <li style="margin:15px; float:left; list-style:none">
                                          <input type="checkbox" name="skills[]" value="HTML" > HTML
                                      </li>
                                      <li style="margin:15px; float:left; list-style:none">
                                          <input type="checkbox" name="skills[]" value="CSS" > CSS
                                      </li>
                                      <li style="margin:15px; float:left; list-style:none">
                                          <input type="checkbox" name="skills[]" value="PHP" > PHP
                                      </li>
                                  </div>

                                  <h4 id="job_head">Requirements</h4>
                                  <div class="form-group">
                                      <textarea rows="3" cols="40" name="requirements" class="form-control"></textarea>
                                  </div>

                                  <h4 id="job_head">company/contact Email</h4>
                                  <input type="email" name="contact_email" class="form-control"><br/>

                                  <input type="submit" value="Enviar"  class="btn btn-primary">
                              </form>






                         </div>
                          </div>
                 </div>

                 <div class="col-md-6">
                     <div class="card">
                         <div class="header">
                           <h4 class="title">Heading here</h4>
                           <p class="category">sub heading here</p>
                         </div>
                         <div class="content">

                             <div class="footer">
                                 <div class="legend">

                                  </div>
                                 <hr>
                                 <div class="stats">

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>



         </div>
     </div>
     @endsection
