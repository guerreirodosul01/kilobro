<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Kilobro</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Fontawesome Icon font -->
    <script src="https://kit.fontawesome.com/1aa5c5e1ba.js"></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Kilobro </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    @auth
                        <li><a class="nav-link" href="{{ url('/findFriends') }}">Encontrar Amigos</a></li>
                        <li><a class="nav-link" href="{{ url('/pedidos') }}">Pedidos de Amizade(
                                {{ App\Amizade::where('status','=',0)->where('id_Para','=',Auth::user()->id)->count() }}
                                )</a></li>

                    @endauth
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{ asset("img/".Auth::user()->imagem) }}" width="30px" height="30px"
                                     class="rounded-circle"> <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item"
                                   href="{{ url('/profile') }}/{{ Auth::user()->slug }}">Perfil</a>

                                <a class="dropdown-item" href="{{ url('editProfile') }}">
                                    Editar Perfil
                                </a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        <li><a class="nav-link" href="{{ url('/amigos') }}"><i class="fas fa-users fa-2x"></i></a></li>

                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link " data-toggle="dropdown"
                               role="button" aria-expanded="false">
                                <i class="fas fa-globe fa-2x"></i>
                                <span class="badge badge-secondary"
                                      style="background:red; position: relative; top: -15px; left:-10px">
                                 {{App\notificacao::where('status', 1)
                                     ->where('user_hero', Auth::user()->id)
                                      ->count()}}
                                    </span>
                            </a>
                            <?php
                            $notes = DB::table('users')
                                ->leftJoin('notificacoes', 'users.id', 'notificacoes.user_logged')
                                ->where('user_hero', Auth::user()->id)
                                ->orderBy('notificacoes.created_at', 'desc')
                                ->get();
                            ?>
                            <div class="dropdown-menu dropdown-menu-right" style="padding: 10px "
                                 aria-labelledby="navbarDropdown">

                                @foreach($notes as $note)
                                    @if($note->status==1)
                                        <a href="{{url('/notificacoes')}}/{{$note->id}}" class="dropdown-item"
                                           style="background:#E4E9F2; padding:10px">
                                            @else
                                                <a href="{{url('/notificacoes')}}/{{$note->id}}" class="dropdown-item"
                                                   style="padding:10px">
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img src="{{ asset("img/".$note->imagem) }}"
                                                                 style="width:50px; padding:5px; background:#fff; border:1px solid #eee"
                                                                 class="card-img">
                                                        </div>

                                                        <div class="col-md-9" style="padding-right: 100px;">

                                                            <b style="color:green; font-size:90%">{{ucwords($note->name)}}</b>
                                                            <br>
                                                            <span
                                                                style="color:#000; font-size:90%">{{$note->note}}</span>
                                                            <br/>
                                                            <small style="color:#90949C"><i aria-hidden="true"
                                                                                            class="fas fa-users"></i>
                                                                {{date('F j, Y', strtotime($note->created_at))}}
                                                                as {{date('H: i', strtotime($note->created_at))}}
                                                            </small>
                                                        </div>

                                                    </div>
                                                </a>
                                        @endforeach

                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
@yield('script')

</html>
