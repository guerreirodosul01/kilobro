<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>Kilobro </title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords"
          content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('\files\assets\images\favicon.ico') }}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('\files\bower_components\bootstrap\css\bootstrap.min.css') }}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\icon\themify-icons\themify-icons.css') }}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\icon\icofont\css\icofont.css') }}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\icon\feather\css\feather.css') }}">
    <!-- Syntax highlighter Prism css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\pages\prism\prism.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\css\style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\css\jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('\files\assets\css\pcoded-horizontal.min.css') }}">
    @yield('css')

    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
</head>
<!-- Menu horizontal fixed layout -->

<body>

<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->

<div id="pcoded" class="pcoded">

    <div class="pcoded-container">
        <!-- Menu header start -->
        <nav class="navbar header-navbar pcoded-header" header-theme="theme4">
            <div class="navbar-wrapper">

                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="feather icon-menu"></i>
                    </a>
                    <a href="/" style="margin-left: 50px">
                        KILOBRO
                    </a>
                    <a class="mobile-options">
                        <i class="feather icon-more-horizontal"></i>
                    </a>
                </div>

                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li class="header-search">
                            <div class="main-search morphsearch-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                    <form action="{{ route("pesquisa") }}" method="get">

                                        <input name="pesquisa" type="text" class="form-control"
                                               placeholder="o que deseja procurar?"
                                               v-model="qry" v-on:Keyup="autoComplete"/>
                                    </form>
                                    <span class="input-group-addon search-btn"><i
                                            class="feather icon-search"></i></span>


                                </div>

                            </div>

                        </li>
                        <li>

                        </li>
                    </ul>
                    <ul class="nav-right">
                        @auth
                            <li class="user-profile header-notification">
                                <a href="{{ route('rankbro') }}" class="btn " style="padding: 5px">
                                    RankBRO
                                </a>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <span>Publicar</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu"
                                        data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                                        <li>
                                            <a href="{{ route('fazercodigo') }}" class="btn " style="padding: 5px">
                                                Publicar Codigo
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('fazerpergunta') }}" class="btn " style="padding: 5px">
                                                Publicar Pergunta
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('fazerartigo') }}" class="btn " style="padding: 5px">
                                                Publicar Artigos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('criartrabalho') }}" class="btn " style="padding: 5px">
                                                Publicar Trabalho
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>

                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        @if(App\notificacao::where('status', 1)
                                     ->where('user_hero', Auth::user()->id)
                                      ->count() > 0)
                                        <span class="badge bg-c-pink">{{App\notificacao::where('status', 1)
                                     ->where('user_hero', Auth::user()->id)
                                      ->count()}}</span>
                                            @endif
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu"
                                        data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <?php
                                        $notes = DB::table('users')
                                            ->leftJoin('notificacoes', 'users.id', 'notificacoes.user_logged')
                                            ->where('user_hero', Auth::user()->id)
                                            ->orderBy('notificacoes.created_at', 'desc')
                                            ->get();
                                        ?>
                                        <li>
                                            <h6>Notificações</h6>
                                        </li>
                                        @foreach($notes as $note)

                                            <li>
                                                <a href="{{url('/notificacoes')}}/{{$note->id}}">
                                                    @if($note->status==1)
                                                        <div class="media">
                                                            <img class="d-flex align-self-center img-radius"
                                                                 src="{{ asset("img/".$note->imagem) }}"
                                                                 style="width: 30px">
                                                            <div class="media-body">
                                                                <h5 class="notification-user">{{ucwords($note->name)}}</h5>
                                                                <p class="notification-msg">{{$note->note}}</p>
                                                                <span class="notification-time">{{date('F j, Y', strtotime($note->created_at))}}
                                                                as {{date('H: i', strtotime($note->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="media">
                                                            <img class="d-flex align-self-center img-radius"
                                                                 src="{{ asset("img/".$note->imagem) }}"
                                                                 alt="Generic placeholder image">
                                                            <div class="media-body">
                                                                <h5 class="notification-user">{{ucwords($note->name)}}</h5>
                                                                <p class="notification-msg">{{$note->note}}</p>
                                                                <span class="notification-time">{{date('F j, Y', strtotime($note->created_at))}}
                                                                as {{date('H: i', strtotime($note->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </a>
                                            </li>


                                        @endforeach


                                    </ul>
                                </div>
                            </li>
                            <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="menssagens displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <input hidden id="idUser" value="{{ Auth::user()->id }}">
                                        <i class="feather icon-message-square"></i>
                                        <div v-if="mensnoti > 0">
                                        <span class="badge bg-c-green">@{{ mensnoti }}</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{ asset("img/".Auth::user()->imagem)}}" style="height: 40px"
                                             class="img-radius"
                                             alt="User-Profile-Image">
                                        <span>{{Auth::user()->name}}</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu"
                                        data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

                                        <li>
                                            <a href="{{ url('/profile') }}/{{ Auth::user()->slug }}">
                                                <i class="feather icon-user"></i> Perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('pedidos') }}">
                                                <i class="feather icon-user"></i> Pedidos de Amizade
                                            </a>
                                        </li>


                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                        @else
                            <li class="header-notification">
                                <a href="{{ route('login') }}">Entrar</a>
                            </li>
                            <li class="header-notification">
                                <a href="{{ route('register') }}">Registrar</a>
                            </li>


                        @endauth
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Menu header end -->
        <div class="pcoded-main-container">
            <nav class="pcoded-navbar">
                <div class="pcoded-inner-navbar">
                    <ul class="pcoded-item pcoded-left-item">
                        <li class="pcoded-hasmenu">
                            <h5 style="margin-top: 14px;margin-left: 18px">

                                <span class="pcoded-mtext">Pesquisas pré-definidas:</span>

                            </h5>
                        </li>
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="feather icon-map"></i></span>
                                <span class="pcoded-mtext">Linguagens de WEB</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">

                                <li class=""><a href="/pesquisa?pesquisa=html"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">HTML</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=css"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">CSS</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=javascript"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">JavaScript</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=php"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">PHP</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=jquery"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Jquery</span> <span
                                            class="pcoded-mcaret"></span> </a></li>


                            </ul>
                        </li>
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="feather icon-map"></i></span>
                                <span class="pcoded-mtext">Linguagens Famosas</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">

                                <li class=""><a href="/pesquisa?pesquisa=java"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Java</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=c"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">C</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=c++"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">C++</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=c#"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">C#</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=ruby"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Ruby</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=lua"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Lua</span> <span
                                            class="pcoded-mcaret"></span> </a></li>

                            </ul>
                        </li>
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="feather icon-map"></i></span>
                                <span class="pcoded-mtext">Banco de Dados</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">

                                <li class=""><a href="/pesquisa?pesquisa=sql"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">SQL</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=mysql"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">MySQL</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=oracle"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Oracle</span> <span
                                            class="pcoded-mcaret"></span> </a></li>


                            </ul>
                        </li>
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="feather icon-map"></i></span>
                                <span class="pcoded-mtext">Frameworks</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">

                                <li class=""><a href="/pesquisa?pesquisa=laravel"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Laravel</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=bootstrap"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Bootstrap</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=materialize"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Materialize</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=vuejs"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">VueJs</span> <span
                                            class="pcoded-mcaret"></span> </a></li>

                            </ul>
                        </li>
                        <li class="pcoded-hasmenu">
                            <a href="javascript:void(0)">
                                <span class="pcoded-micon"><i class="feather icon-map"></i></span>
                                <span class="pcoded-mtext">Outros Assuntos Famosos</span>
                                <span class="pcoded-mcaret"></span>
                            </a>
                            <ul class="pcoded-submenu">

                                <li class=""><a href="/pesquisa?pesquisa=engenharia"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext"
                                            data-i18n="nav.navigate.main">Engenharia de Software</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=redes"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Redes</span> <span
                                            class="pcoded-mcaret"></span> </a></li>
                                <li class=""><a href="/pesquisa?pesquisa=projetos"> <span class="pcoded-micon"><i
                                                class="ti-layout-cta-right"></i><b>N</b></span> <span
                                            class="pcoded-mtext" data-i18n="nav.navigate.main">Projetos</span> <span
                                            class="pcoded-mcaret"></span> </a></li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>

        @auth
            <!-- Sidebar chat start -->
                <span id="menssagens">
                <div id="sidebar" class="users p-chat-user showChat">
                    <div class="had-container">
                        <div class="card card_main p-fixed users-main">
                            <div class="user-box">
                                <br>
                                <br>
                                <div class="main-friend-list">
                                    <div class="userlist-box">
                                        <div v-if="privsteMsgs.length > 0">
                                        <div class="media" v-for="privsteMsg in privsteMsgs"
                                             @click="messages(privsteMsg.id)">
                                            <a class="media-left">
                                                <img class="media-object img-radius img-radius"
                                                     :src="'img/' + privsteMsg.imagem" alt="Generic placeholder image ">

                                            </a>
                                            <div class="media-body">
                                                <div class="f-13 chat-header">@{{ privsteMsg.name }}</div>
                                            </div>
                                        </div>
                                        </div>
                                        <div v-else>
                                            <div class="media" >
                                            <div class="media-body">
                                                <div class="f-13 chat-header">Você ainda não tem amigos para conversar</div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Sidebar inner chat start-->
                <div class="showChat_inner">
                    <div class="media chat-inner-header">
                        <a class="back_chatBox">
                            <i class="feather icon-chevron-left"></i>
                        </a>
                        <br>
                    </div>
                    <div id="mens" style="overflow: scroll;height:70%;overflow:auto">
                        <div id="tammens">
                    <div class="" v-for="singleMsg in singleMsgs">
                        <div class="media chat-messages" v-if="singleMsg.id_De != <?php echo Auth::user()->id; ?> ">
                            <a class="media-left photo-table" href="">
                                <img class="media-object img-radius img-radius m-t-5" :src="'img/' + singleMsg.imagem"
                                     alt="Generic placeholder image">
                            </a>
                            <div class="media-body chat-menu-content">

                                <p class="chat-cont">@{{ singleMsg.menssagem }}</p>
                                <p class="chat-time">@{{ singleMsg.created_at | myOwnTime }}</p>

                            </div>
                        </div>
                        <div class="media chat-messages" v-else>
                            <div class="media-body chat-menu-reply">

                                <p class="chat-cont">@{{ singleMsg.menssagem }}</p>
                                <p class="chat-time">@{{ singleMsg.created_at | myOwnTime  }}</p>

                            </div>
                            <div class="media-right photo-table">
                                <a href="">
                                    <img class="media-object img-radius img-radius m-t-5"
                                         :src="'img/' + singleMsg.imagem" alt="Generic placeholder image">
                                </a>
                            </div>
                        </div>
                    </div>
                            </div>
                    </div>
                    <div class="chat-reply-box p-b-20" style="margin-bottom: 20px">
                        <div class="right-icon-control">
                            <form>
                                @csrf
                                <input type="text" class="form-control search-text" placeholder="Enviar menssagem"
                                       v-model="msgFrom" @keydown="inputHandler">
                                <div class="form-icon">
                                    <i class="feather icon-navigation"></i>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </span>
                <!-- Sidebar inner chat end-->
                <div class="pcoded-wrapper">
                    @endauth

                    @yield('content')
                </div>


        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/moment-with-locales.min.js') }}"></script>
<!-- Required Jquery -->

<script type="text/javascript" src="{{ asset('\files\bower_components\jquery\js\jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('\files\bower_components\jquery-ui\js\jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('\files\bower_components\popper.js\js\popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('\files\bower_components\bootstrap\js\bootstrap.min.js') }}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript"
        src="{{ asset('\files\bower_components\jquery-slimscroll\js\jquery.slimscroll.js') }}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{ asset('\files\bower_components\modernizr\js\modernizr.js') }}"></script>
<script type="text/javascript" src="{{ asset('\files\bower_components\modernizr\js\css-scrollbars.js') }}"></script>

<!-- Syntax highlighter prism js -->
<script type="text/javascript" src="{{ asset('\files\assets\pages\prism\custom-prism.js') }}"></script>
<!-- i18next.min.js -->
<script type="text/javascript" src="{{ asset('\files\bower_components\i18next\js\i18next.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('\files\bower_components\i18next-xhr-backend\js\i18nextXHRBackend.min.js') }}"></script>

<script type="text/javascript"
        src="{{ asset('\files\bower_components\i18next-browser-languagedetector\js\i18nextBrowserLanguageDetector.min.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('\files\bower_components\jquery-i18next\js\jquery-i18next.min.js') }}"></script>

<!-- Custom js -->

<script src="{{ asset('\files\assets\js\pcoded.min.js') }}"></script>

<script src="{{ asset('\files\assets\js\menu\menu-hori-fixed.js') }}"></script>
@yield('script')
@auth
    <script src="{{mix('js/profile.js')}}" type="text/javascript"></script>
@endauth
<script src="{{ asset('\files\assets\js\jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('\files\assets\js\script.js') }}"></script>


</body>


</html>
