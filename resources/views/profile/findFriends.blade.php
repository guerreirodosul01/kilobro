@extends('profile.master')

@section('content')

    <div class="container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/profile') }}/{{Auth::user()->slug}}">Perfil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Encontrar Amigos</li>
            </ol>
        </nav>

        <div class="row justify-content-center">
            @include('profile.sidebar')


            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Encontrar Amigos</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            @foreach($allUsers as $uList)
                                <div class="col-sm-4 col-md-4">

                                    <div class="card" style="">
                                        <img class="card-img-top" src="{{ asset("img/".$uList->imagem) }}"
                                             alt="Card image cap" width="100px" height="200px">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ucwords($uList->name)}}</h5>
                                            <p class="card-text">{{ $uList->cidade }} - {{$uList->pais}}</p>
                                            <?php
                                            $check = DB::table('amizades')
                                            ->where('id_Para','=',$uList->id)
                                            ->where('id_De','=',Auth::user()->id)
                                            ->first();
                                            if(!$check){
                                            ?>
                                            <a href="{{url('/addFriend')}}/{{$uList->id}}" class="btn btn-success">Adicionar</a>
                                            <?php } else { ?>
                                            <p class="btn btn-info" aria-disabled="true">Pedido Enviado</p>

                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
