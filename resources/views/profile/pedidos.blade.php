@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <div class="sub-title">Pedidos</div>
                                @if (session('msg'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('msg') }}
                                    </div>
                                @endif
                                <div class="tab-content tabs card-block">
                                    <h4 class="m-b-20"><b>{{ count($users) }}</b> Resultados encontrados</h4>
                                    <div class="row">
                                        <!-- Search result found start -->
                                        <div class="col-lg-12 col-xl-12 search2 search-result">
                                            <div class="card card-main">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            @foreach($users as $user)
                                                                <div class="search-content"
                                                                     style=" margin-bottom:10px;margin-top: 10px">
                                                                    <img class="card-img-top"
                                                                         src="{{asset('img/'.$user->imagem)}}"
                                                                         style="width: 50px">
                                                                    <div class="card-block">


                                                                        <a href="profile/{{$user->slug}}">
                                                                            <h5 class="card-title">{{$user->name}}

                                                                            </h5>
                                                                        </a>

                                                                        <br>
                                                                        <a href="{{url('/acceptFriend')}}/{{$user->name}}/{{$user->id}}" class="btn btn-success">Aceitar Pedido</a>

                                                                        <a href="{{url('/recusarFriend')}}/{{$user->id}}" class="btn btn-danger">Recusar Pedido</a>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Search result found end -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/pergunta.js')}}" type="text/javascript"></script>
@endsection
