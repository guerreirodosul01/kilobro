@extends('layouts.user')

@section('content')
    <div class="main-body container">
        <div class="page-wrapper">
            <br>
            <!-- Page-body start -->
            <div class="page-body" id="app">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <div class="content social-timeline">
                                <div class="">
                                    <!-- Row Starts -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- Social wallpaper start -->
                                            <div class="social-wallpaper">
                                                <img src="..\files\assets\images\social\img1.jpg"
                                                     class="img-fluid width-100" alt="">

                                            </div>
                                            <!-- Social wallpaper end -->

                                        </div>
                                    </div>
                                    <!-- Row end -->
                                    <!-- Row Starts -->
                                    <div class="row">
                                        <div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">
                                            <!-- Social timeline left start -->
                                            <div class="social-timeline-left">
                                                <!-- social-profile card start -->
                                                <div class="card">
                                                    <div class="social-profile">
                                                        <img class="img-fluid width-100"
                                                             src="{{ asset("img/".$userData[0]->imagem) }}" alt="">
                                                        @if(Auth::check() && $userData[0]->slug == Auth::user()->slug)
                                                            <div class="profile-hvr m-t-15">
                                                                <i data-toggle="modal" data-target="#mudafoto"
                                                                   class="icofont icofont-ui-edit p-r-10"></i>
                                                            </div>
                                                            <div class="modal fade"
                                                                 id="mudafoto"
                                                                 role="dialog">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div
                                                                            class="modal-header">
                                                                            <h4 class="modal-title">
                                                                                Mudar Foto</h4>
                                                                            <button
                                                                                type="button"
                                                                                class="close"
                                                                                data-dismiss="modal">
                                                                                &times;
                                                                            </button>

                                                                        </div>
                                                                        <form action="/uploadFoto" method="post"
                                                                              enctype="multipart/form-data">
                                                                            <div class="modal-body">

                                                                                @csrf
                                                                                <input type="file" name="imagem"
                                                                                       class="form-control">
                                                                                <br>


                                                                            </div>
                                                                            <div
                                                                                class="modal-footer">
                                                                                <button
                                                                                    type="button"
                                                                                    class="btn btn-danger waves-effect waves-light f-left"
                                                                                    data-dismiss="modal">
                                                                                    Fechar
                                                                                </button>
                                                                                <input type="submit"
                                                                                       class="btn btn-primary waves-effect waves-light f-right"
                                                                                       name="btn" value="enviar">
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="card-block social-follower">
                                                        <h4>{{ $userData[0]->name }}</h4>
                                                        <div class=" follower-counter text-center">
                                                            BroPoints: {{$userData[0]->rankpontos}}
                                                        </div>
                                                        @if(Auth::check() && $userData[0]->slug != Auth::user()->slug)
                                                            <?php
                                                            $existepedido1 = DB::table('amizades')
                                                                ->where('id_De', '=', $userData[0]->id)
                                                                ->where('id_Para', '=', Auth::user()->id)
                                                                ->where('status', '=', 0)
                                                                ->first();
                                                            $existepedido2 = DB::table('amizades')
                                                                ->where('id_Para', '=', $userData[0]->id)
                                                                ->where('id_De', '=', Auth::user()->id)
                                                                ->where('status', '=', 0)
                                                                ->first();
                                                            $amigos1 = DB::table('amizades')
                                                                ->where('id_De', '=', $userData[0]->id)
                                                                ->where('id_Para', '=', Auth::user()->id)
                                                                ->where('status', '=', 1)
                                                                ->first();
                                                            $amigos2 = DB::table('amizades')
                                                                ->where('id_Para', '=', $userData[0]->id)
                                                                ->where('id_De', '=', Auth::user()->id)
                                                                ->where('status', '=', 1)
                                                                ->first();

                                                            if(!$existepedido1 && !$existepedido2 && !$amigos1 && !$amigos2){
                                                            ?>
                                                            <div class="">
                                                                <a href="{{url('/addFriend')}}/{{$userData[0]->id}}"
                                                                   class="btn btn-outline-primary waves-effect btn-block">
                                                                    <i class="icofont icofont-ui-user m-r-10"></i>
                                                                    Adicionar
                                                                    como amigo
                                                                </a>
                                                            </div>
                                                            <?php } ?>

                                                            <?php if($existepedido2){ ?>
                                                            <div class="">
                                                                <a
                                                                    class="btn btn-outline-primary waves-effect btn-block">
                                                                    <i class="icofont icofont-ui-user m-r-10"></i>
                                                                    Pedido Enviado
                                                                </a>
                                                            </div>
                                                            <?php } ?>

                                                                <?php if($existepedido1){ ?>
                                                                <div class="">

                                                                    <a href="{{url('/acceptFriend')}}/{{$userData[0]->name}}/{{$userData[0]->id}}" class="btn btn-outline-success waves-effect btn-block">
                                                                        <i class="icofont icofont-ui-user m-r-10"></i>Aceitar Pedido</a>
<br>
                                                                    <a href="{{url('/recusarFriend')}}/{{$userData[0]->id}}" class="btn btn-outline-danger waves-effect btn-block">
                                                                        <i class="icofont icofont-ui-user m-r-10"></i>Recusar Pedido</a>
                                                                </div>
                                                                <?php } ?>
                                                                <?php if($amigos1 || $amigos2 ){ ?>
                                                                <div class="">
                                                                    <a href="{{url('/desfazerAmizade')}}/{{$userData[0]->id}}" class="btn btn-outline-danger waves-effect btn-block">
                                                                        <i class="icofont icofont-ui-user m-r-10"></i>Remover Amigo</a>
                                                                </div>
                                                                <?php } ?>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!-- social-profile card end -->

                                                <!-- Friends card start -->
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5 class="card-header-text d-inline-block">Amigos</h5>
                                                    </div>
                                                    <div class="card-block friend-box">
                                                        @foreach($friends as $uList)
                                                            <a href="/profile/{{ucwords($uList->slug)}}">
                                                                <img class="media-object img-radius"
                                                                     src="{{ asset("img/".$uList->imagem) }}" alt=""
                                                                     data-toggle="tooltip" data-placement="top" title=""
                                                                     data-original-title="{{ucwords($uList->name)}}">
                                                            </a>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <!-- Friends card end -->
                                            </div>
                                            <!-- Social timeline left end -->
                                        </div>
                                        <div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">
                                            <!-- Nav tabs -->
                                            <div class="card social-tabs">
                                                <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#timeline"
                                                           role="tab" aria-expanded="true">Linha do tempo</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#about" role="tab"
                                                           aria-expanded="false">Sobre</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#friends" role="tab"
                                                           aria-expanded="false">Amigos</a>
                                                        <div class="slide"></div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <!-- Timeline tab start -->
                                                <div class="tab-pane active" id="timeline" aria-expanded="true">
                                                    <div class="row">
                                                        @if(Auth::check() && $userData[0]->slug == Auth::user()->slug)

                                                            <div class="card bg-white col-md-9  m-auto">
                                                                <div v-if="!image">
                                                                    <form method="post" enctype="multipart/form-data"
                                                                          v-on:submit.prevent="addPost">
                                                                        <div class="post-new-contain row card-block">
                                                                            <div class="col-md-1 col-xs-3 post-profile">
                                                                                <img
                                                                                    src="{{ asset("img/".$userData[0]->imagem) }}"
                                                                                    style="height: 50px" alt="">
                                                                            </div>

                                                                            <div class="col-md-11 col-xs-9">
                        <textarea v-model="content" id="post-message" class="form-control post-input" rows="3" cols="10"
                                  placeholder="O que você está pensando ?" required=""></textarea>

                                                                            </div>

                                                                        </div>
                                                                        <div class="post-new-footer b-t-muted p-15">
                <span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title=""
                      data-original-title="Add Photos">
                                          <label for="file-input" class="file-upload-lbl">
                                            <i class="icofont icofont-image text-muted"></i>
                                          </label>

                                          <input @change="onFileChange" id="file-input" type="file"
                                                 accept="image/x-png,image/gif,image/jpeg">
                                        </span>
                                                                            <input type="submit" id="post-new"
                                                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                                                   value="Postar">
                                                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                                <div v-else>
                                                                    <form method="post" enctype="multipart/form-data"
                                                                          v-on:submit.prevent="uploadImg">
                                                                        <div class="post-new-contain row card-block">
                                                                            <div class="col-md-1 col-xs-3 post-profile">
                                                                                <img
                                                                                    src="{{ asset("img/".$userData[0]->imagem) }}"
                                                                                    style="height: 50px" alt="">
                                                                            </div>

                                                                            <div class="col-md-11 col-xs-9">
                        <textarea v-model="content" id="post-message" class="form-control post-input" rows="3" cols="10"
                                  placeholder="O que você está pensando ?" required=""></textarea>

                                                                            </div>
                                                                            <div class="col-md-12 col-xs-9">
                                                                                <img :src="image"
                                                                                     style="display:block;width:80%; margin:auto;margin-top: 20px;margin-bottom: 20px"/><br>
                                                                            </div>

                                                                        </div>
                                                                        <div class="post-new-footer b-t-muted p-15">

                                                                            <label for="file-input"
                                                                                   class="file-upload-lbl"
                                                                                   @click="removeImg">
                                                                                <i class="icofont icofont-close text-muted"></i>
                                                                            </label>


                                                                            </span>
                                                                            <input type="submit" id="post-new"
                                                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                                                   value="Postar">
                                                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>

                                                        @endif
                                                        <div v-for="post,key in posts" class="col-md-12 timeline-dot">
                                                            <div class="social-timelines p-relative">
                                                                <div class="row timeline-right p-t-35">
                                                                    <div class="col-2 col-sm-2 col-xl-1">
                                                                        <div class="social-timelines-left">
                                                                            <img class="img-radius timeline-icon"
                                                                                 :src="'../../img/' + post.user.imagem"
                                                                                 alt="">
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="col-10 col-sm-10 col-xl-11 p-l-5 p-b-35">
                                                                        <div class="card">

                                                                            <div class="card-block post-timelines">
                                                                                @if(Auth::check())
                                                                                    <div class=" wall-elips">
                        <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false" role="tooltip"></span>
                                                                                        <div
                                                                                            class="dropdown-menu dropdown-menu-right b-none services-list"
                                                                                            x-placement="bottom-end"
                                                                                            style="position: absolute; transform: translate3d(550px, 41px, 0px); top: 20px; left: 0px; will-change: transform;">
                                                                                            <li class="dropdown-item"
                                                                                                v-if="post.user_id == '{{Auth::user()->id}}'">
                                                                                                <a
                                                                                                    data-toggle="modal"
                                                                                                     :data-target="'#myModal' + post.id"
                                                                                                    @click="openModal(post.id)">Editar</a>
                                                                                            </li>
                                                                                            <li class="dropdown-item"
                                                                                                v-if="post.user_id == '{{Auth::user()->id}}'">
                                                                                                <a @click="deletePost(post.id)">
                                                                                                    <i class="fa fa-trash"></i>
                                                                                                    Delete</a>
                                                                                            </li>
                                                                                            <!-- Modal content-->


                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="modal fade"
                                                                                         :id="'myModal'+ post.id"
                                                                                         role="dialog">
                                                                                        <div class="modal-dialog">
                                                                                            <div class="modal-content">
                                                                                                <div
                                                                                                    class="modal-header">
                                                                                                    <h4 class="modal-title">
                                                                                                        Editar Post</h4>
                                                                                                    <button
                                                                                                        type="button"
                                                                                                        class="close"
                                                                                                        data-dismiss="modal">
                                                                                                        &times;
                                                                                                    </button>

                                                                                                </div>
                                                                                                <div class="modal-body">
                                    <textarea v-model="updatedContent" id="post-message" class="form-control post-input"
                                              rows="3" cols="10"
                                              placeholder="O que você está pensando ?"
                                              required="">@{{post.content}}</textarea>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="modal-footer">
                                                                                                    <button
                                                                                                        type="button"
                                                                                                        class="btn btn-danger waves-effect waves-light f-left"
                                                                                                        data-dismiss="modal">
                                                                                                        Fechar
                                                                                                    </button>
                                                                                                    <button
                                                                                                        type="button"
                                                                                                        id="post-new"
                                                                                                        class="btn btn-primary waves-effect waves-light f-right"
                                                                                                        data-dismiss="modal"
                                                                                                        @click="updatePost(post.id)">
                                                                                                        Salvar
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                <div class="chat-header f-w-600">
                                                                                    @{{post.user.name}} postou em sua
                                                                                    linha do
                                                                                    tempo
                                                                                </div>

                                                                                <div class="social-time text-muted">@{{
                                                                                    post.created_at | myOwnTime }}
                                                                                </div>


                                                                            </div>
                                                                            <div id="lightgallery"
                                                                                 class="lightgallery-popup">

                                                                                <img v-if="post.image"
                                                                                     :src="'../../img/' + post.image"
                                                                                     class="img-fluid width-100" alt="">

                                                                            </div>
                                                                            <div class="card-block">
                                                                                <div class="timeline-details">
                                                                                    <p class="text-muted">
                                                                                        @{{post.content}}</p>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                class="card-block b-b-theme b-t-theme social-msg">
                                                                                <a style="margin-right: 10px">


                                                                                    @if(Auth::check())
                                                                                        <div
                                                                                            v-if="!post.listlikes.includes( {{Auth::user()->id}})">
                                                                                            <i class="icofont icofont-heart-alt text-muted">

                                                                                            </i>
                                                                                            <i
                                                                                                @click="likePost(post.id)">Like
                                                                                                (@{{post.likes.length}})</i>

                                                                                        </div>
                                                                                        <div v-else>
                                                                                            <i class="icofont icofont-heart-alt text-muted">

                                                                                            </i>

                                                                                            <i @click="deslikePost(post.id)">Deslike (@{{post.likes.length}})</i>
                                                                                        </div>
                                                                                    @else
                                                                                        <i class="icofont icofont-heart-alt text-muted">

                                                                                        </i>
                                                                                        <span>Like (@{{post.likes.length}})</span>
                                                                                    @endif
                                                                                </a>

                                                                                <i class="icofont icofont-comment text-muted">
                                                                                </i>
                                                                                <span onclick="var no = this.parentNode.nextSibling.nextSibling;if (no.style.display === 'none') {
                                        no.style.display = 'block';
                                        } else {
                                        no.style.display = 'none';
                                        }">Comentarios (@{{post.comments.length}})</span>


                                                                                <!--<img class="media-object img-radius m-r-10" src="../files/assets/images/avatar-1.jpg" alt="">-->
                                                                                <!--<img class="media-object img-radius m-r-10" src="../files/assets/images/avatar-1.jpg" alt="">-->
                                                                                <!---->
                                                                            </div>
                                                                            <div class="card-block user-box"
                                                                                 style="display: none;"
                                                                                 :id="'commentSeen'+key">
                                                                                <div class="p-b-20">
                                                                        <span class="f-14">Comentarios (@{{post.comments.length}})
                                        </span>

                                                                                </div>
                                                                                <div class="media col-md-12"
                                                                                     v-for="comment in post.comments">
                                                                                    @if(Auth::check())
                                                                                        <div class="media col-md-12"
                                                                                             v-if="comment.user_id=={{Auth::user()->id}}">
                                                                                            <a class="media-left"
                                                                                               :href="'{{url('/profile')}}/' + post.user.slug">
                                                                                                <img
                                                                                                    class="media-object img-radius m-r-20"
                                                                                                    src="..\files\assets\images\avatar-1.jpg"
                                                                                                    alt="Generic placeholder image">
                                                                                            </a>
                                                                                            <div
                                                                                                class="media-body b-b-theme social-client-description">
                                                                                                <a :href="'{{url('/profile')}}/' + post.user.slug">
                                                                                                    <div
                                                                                                        class="chat-header">
                                                                                                        @{{post.user.name}}
                                                                                                    </div>
                                                                                                </a>
                                                                                                <p class="text-muted">
                                                                                                    @{{comment.comment}}</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="media col-md-12"
                                                                                             v-else>
                                                                                            <a class="media-left"
                                                                                               :href="'{{url('/profile')}}/' + post.user.slug">
                                                                                                <img
                                                                                                    class="media-object img-radius m-r-20"
                                                                                                    src="..\files\assets\images\avatar-1.jpg"
                                                                                                    alt="Generic placeholder image">
                                                                                            </a>
                                                                                            <div
                                                                                                class="media-body b-b-theme social-client-description">
                                                                                                <div
                                                                                                    class="chat-header">
                                                                                                    @{{post.user.name}}
                                                                                                </div>
                                                                                                <p class="text-muted">
                                                                                                    @{{comment.comment}}</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    @else
                                                                                        <div class="media col-md-12">
                                                                                            <a class="media-left"
                                                                                               :href="'{{url('/profile')}}/' + post.user.slug">
                                                                                                <img
                                                                                                    class="media-object img-radius m-r-20"
                                                                                                    src="..\files\assets\images\avatar-1.jpg"
                                                                                                    alt="Generic placeholder image">
                                                                                            </a>
                                                                                            <div
                                                                                                class="media-body b-b-theme social-client-description">
                                                                                                <div
                                                                                                    class="chat-header">
                                                                                                    @{{post.user.name}}
                                                                                                </div>
                                                                                                <p class="text-muted">
                                                                                                    @{{comment.comment}}</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endif
                                                                                </div>

                                                                                @if(Auth::check())
                                                                                    <div class="media">

                                                                                        <img
                                                                                            class="media-object img-radius m-r-20"
                                                                                            src="../../img/{{Auth::user()->imagem}}"
                                                                                            alt="Generic placeholder image">

                                                                                        <div class="media-body">
                                                                                            <form class="">
                                                                                                <div class="">
                                        <textarea class="f-13 form-control msg-send" rows="3" cols="10" required=""
                                                  placeholder="Comentar....." v-model="commentData[key]"></textarea>
                                                                                                    <div
                                                                                                        class="text-right m-t-20">
                                                                                                        <a @click="addComment(post,key)"
                                                                                                           class="btn btn-primary waves-effect waves-light">Enviar</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Timeline tab end -->
                                                <!-- About tab start -->
                                                <div class="tab-pane" id="about" aria-expanded="false">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <h5 class="card-header-text">Informação básica</h5>
                                                                    @if(Auth::check() && $userData[0]->slug == Auth::user()->slug)
                                                                    <button data-toggle="modal" data-target="#mudasobre" id="edit-btn" type="button"
                                                                            class="btn btn-primary waves-effect waves-light f-right">
                                                                        <i class="icofont icofont-edit"></i>
                                                                    </button>
                                                                        @endif
                                                                </div>
                                                                <div class="card-block">
                                                                    <div id="view-info" class="row">
                                                                        <div class="col-lg-6 col-md-12">
                                                                            <form>
                                                                                <table
                                                                                    class="table table-responsive m-b-0">
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <th class="social-label b-none p-t-0">
                                                                                            Cidade
                                                                                        </th>
                                                                                        <td class="social-user-name b-none p-t-0 text-muted">
                                                                                            {{ $userData[0]->cidade }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="social-label b-none">
                                                                                            Pais
                                                                                        </th>
                                                                                        <td class="social-user-name b-none text-muted">
                                                                                            {{ $userData[0]->pais }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="social-label b-none">
                                                                                            Sobre
                                                                                        </th>
                                                                                        <td class="social-user-name b-none text-muted">
                                                                                            {{ $userData[0]->sobre }}
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal fade"
                                                                         id="mudasobre"
                                                                         role="dialog">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                                <div
                                                                                    class="modal-header">
                                                                                    <h4 class="modal-title">
                                                                                        Mudar Informações</h4>
                                                                                    <button
                                                                                        type="button"
                                                                                        class="close"
                                                                                        data-dismiss="modal">
                                                                                        &times;
                                                                                    </button>

                                                                                </div>
                                                                                <form action="{{ url('/updateProfile') }}" method="post"
                                                                                      enctype="multipart/form-data">
                                                                                    <div class="modal-body">

                                                                                        @csrf
                                                                                        <div class="form-group">
                                                                                            <label for="cidade"><span class="badge badge-secondary">Cidade</span></label>
                                                                                            <input type="text" class="form-control" id="cidade" name="cidade"
                                                                                                   value="{{$userData[0]->cidade}}">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="pais"><span class="badge badge-secondary">Pais</span></label>
                                                                                            <input type="text" class="form-control" id="pais" name="pais"
                                                                                                   value="{{$userData[0]->pais}}">
                                                                                        </div>
                                                                                        <div class="form-group">
                                                                                            <label for="sobre"><span class="badge badge-secondary">Sobre</span></label>
                                                                                            <textarea class="form-control" id="sobre" name="sobre"
                                                                                                      rows="12">{{$userData[0]->sobre}}</textarea>
                                                                                        </div>


                                                                                    </div>
                                                                                    <div
                                                                                        class="modal-footer">
                                                                                        <button
                                                                                            type="button"
                                                                                            class="btn btn-danger waves-effect waves-light f-left"
                                                                                            data-dismiss="modal">
                                                                                            Fechar
                                                                                        </button>
                                                                                        <input type="submit"
                                                                                               class="btn btn-primary waves-effect waves-light f-right"
                                                                                                value="enviar">
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- About tab end -->
                                                <!-- Friends tab start -->
                                                <div class="tab-pane" id="friends" aria-expanded="false">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-xl-6">
                                                            <div class="card">
                                                                @foreach($friends as $uList)
                                                                    <div class="card-block post-timelines">
                                                                        @if(Auth::check() && $userData[0]->slug == Auth::user()->slug)
                                                                            <span
                                                                                class="dropdown-toggle addon-btn text-muted f-right service-btn"
                                                                                data-toggle="dropdown"
                                                                                aria-haspopup="true"
                                                                                aria-expanded="true"
                                                                                role="tooltip"></span>
                                                                            <div
                                                                                class="dropdown-menu dropdown-menu-right b-none services-list">
                                                                                <a class="dropdown-item"
                                                                                   href="{{url('/desfazerAmizade')}}/{{$uList->id}}">Remover
                                                                                    Amigo </a>
                                                                            </div>
                                                                        @endif
                                                                        <div class="media bg-white d-flex">
                                                                            <div class="media-left media-middle">
                                                                                <a href="/profile/{{ucwords($uList->slug)}}">
                                                                                    <img class="media-object"
                                                                                         src="{{ asset("img/".$uList->imagem) }}"
                                                                                         alt="" style="width: 100px">

                                                                                </a>
                                                                            </div>
                                                                            <div class="media-body friend-elipsis">
                                                                                <div
                                                                                    class="f-15 f-bold m-b-5">{{ucwords($uList->name)}}
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Friends tab end -->
                                            </div>
                                            <!-- Row end -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-body end -->
        </div>
    </div>

@endsection
@section('script')
    <script src="{{mix('js/perfil.js')}}" type="text/javascript"></script>
@endsection
{{--<div class="container">--}}

{{--    <nav aria-label="breadcrumb">--}}
{{--        <ol class="breadcrumb bg-transparent">--}}
{{--            <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>--}}
{{--            <li class="breadcrumb-item active" aria-current="page">Perfil</li>--}}
{{--        </ol>--}}
{{--    </nav>--}}

{{--    <div class="row justify-content-center">--}}
{{--        @include('profile.sidebar')--}}


{{--        @foreach($userData as $uData)--}}
{{--            <div class="col-md-9">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">{{ucwords($uData->name) }}</div>--}}

{{--                    <div class="card-body">--}}
{{--                        @if (session('status'))--}}
{{--                            <div class="alert alert-success" role="alert">--}}
{{--                                {{ session('status') }}--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-sm-4 col-md-4">--}}
{{--                                <div class="card" style="">--}}
{{--                                    <img class="card-img-top" src="{{ asset("img/".Auth::user()->imagem) }}"--}}
{{--                                         alt="Card image cap" width="100px" height="200px">--}}
{{--                                    <div class="card-body">--}}
{{--                                        <h5 class="card-title">{{ucwords($uData->name)}}</h5>--}}
{{--                                        <p class="card-text">{{$uData->cidade}} - {{$uData->pais}}</p>--}}
{{--                                        @if ($uData->user_id == Auth::user()->id)--}}
{{--                                            <a href="{{url('/editProfile')}}" class="btn btn-primary">Editar--}}
{{--                                                Perfil</a>--}}
{{--                                        @endif--}}

{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}

{{--                            <div class="col-sm-8 col-md-8">--}}
{{--                                <h4 class=""><span class="badge badge-secondary">Sobre</span></h4>--}}
{{--                                <p> {{$uData->sobre}} </p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}

{{--    </div>--}}
{{--</div>--}}
