@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <div class="sub-title">Notificação</div>
                                @if (session('msg'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('msg') }}
                                    </div>
                                @endif
                                <div class="tab-content tabs card-block">
                                    <div class="row">
                                        <!-- Search result found start -->
                                        <div class="col-lg-12 col-xl-12 search2 search-result">
                                            <div class="card card-main">
                                                <div class="card-block">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            @foreach($notes as $note)
                                                                <div class="search-content"
                                                                     style=" margin-top: 40px">
                                                                    <div class="card-block">


                                                                        <a href="/profile/{{$note->slug}}">
                                                                            <h5 class="card-title">{{$note->name}}

                                                                            </h5>
                                                                        </a>
                                                                        <p>{{$note->note}}</p>
                                                                        <br>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Search result found end -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/pergunta.js')}}" type="text/javascript"></script>
@endsection
