@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        @if(Auth::check())

                            <div class="card bg-white col-md-12  m-auto">
                                <div>
                                    <form action="{{ route('adicionacodigo') }}" method="post" enctype="multipart/form-data" >
                                        <div class="post-new-contain row card-block">
                                            @csrf
                                            <div class="col-md-12 col-xs-12">
                                                <br>
                                                <h5>Titulo Do Codigo:</h5>
                                                <br>
                                                <input name="titulo" required class="form-control"
                                                       placeholder="O Titulo">
                                                <br>
                                                <h5>Caminho e Extenção Do Codigo:</h5>
                                                <br>
                                                <input name="extencao" required class="form-control"
                                                       placeholder="Ex: site.html">
                                                <br>
                                                <h5>Codigo:</h5>
                                                <br>
                                                <textarea name="conteudo" id="editor" class="form-control "
                                                          rows="30" cols="10"
                                                          placeholder="O que você está pensando ?"
                                                          ></textarea>

                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Publicar">
                                            <br>
                                            <br>
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>

                            </div>

                        @endif
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ URL::to('js/vendor/edit_area/edit_area_full.js')}}" type="text/javascript"></script>

    <script language="Javascript" type="text/javascript">
        editAreaLoader.init({
            id: "editor"	// id of the textarea to transform
            ,start_highlight: true	// if start with highlight
            ,allow_toggle: false
            ,allow_resize: "no"
            ,word_wrap: true
            ,language: "pt"
            ,syntax: "php"
            ,toolbar: "undo, redo, |, select_font, |, syntax_selection, |, highlight, reset_highlight"
            ,show_line_colors: true
            ,syntax_selection_allow: "css,html,js,php,xml"
        });
    </script>
@endsection
