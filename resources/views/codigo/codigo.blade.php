@extends('layouts.user')

@section('content')
    <div class="pcoded-content" style="height: 670px">
        <div class="pcoded-inner-content" style="padding-top: 4%;height: 100%">

            <!-- Main-body start -->
            <p>Link Referencial: {{$caminho[0]->caminho}}</p>
            <p>NOME: {{$caminho[0]->titulo}}</p>
            <div class="main-body" style="width: 100%;height: 100%">
                <iframe src="{{$caminho[0]->caminho}}" height="100%" width="100%"></iframe>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/artigo.js')}}" type="text/javascript"></script>
@endsection
