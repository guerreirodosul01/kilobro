@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        @if(Auth::check())

                            <div class="card bg-white col-md-9  m-auto">
                                <div>
                                    <form action="{{ route('adicionaArtigo') }}" method="post" enctype="multipart/form-data" >
                                        <div class="post-new-contain row card-block">
                                            @csrf
                                            <div class="col-md-12 col-xs-12">
                                                <br>
                                                <h5>Titulo Do Artigo:</h5>
                                                <br>
                                                <input name="titulo" required class="form-control"
                                                       placeholder="O Titulo">
                                                <br>
                                                <h5>Conteudo do Artigo:</h5>
                                                <br>
                                                <textarea name="conteudo" id="post-message" class="form-control "
                                                          rows="30" cols="10"
                                                          placeholder="O que você está pensando ?"
                                                          ></textarea>

                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Publicar">
                                            <br>
                                            <br>
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>

                            </div>

                        @endif
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ URL::to('js/vendor/tinymce/js/tinymce/tinymce.min.js')}}" type="text/javascript"></script>

    <script>
        var editor_config = {
            path_absolute : "{{ URL::to('/') }}/",
            selector : "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media  nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.grtElementByTagName('body')[0].clientHeight;
                var cmsURL = editor_config.path_absolute+'laravel-filemanaget?field_name'+field_name;
                if (type = 'image') {
                    cmsURL = cmsURL+'&type=Images';
                } else {
                    cmsUrl = cmsURL+'&type=Files';
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizeble : 'yes',
                    close_previous : 'no'
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection
