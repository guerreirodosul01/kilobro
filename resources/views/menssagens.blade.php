@extends('profile.master')

@section('content')
    <div id="menssagens">


        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-transparent">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Perfil</li>
                </ol>
            </nav>

            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            @{{msg}}
                        </div>
                        <div class="card-body">
                            <ul style="padding: 0px">
                            <div v-for="privsteMsg in privsteMsgs" style="padding: 0px">
                                <li v-if="privsteMsg.status == 1" @click="messages(privsteMsg.id)" style="list-style: none;margin-top: 10px;background-color: #815cad;width: 220px;padding: 10px">
                                    <img :src="'img/' + privsteMsg.imagem" style="width:50px;height: 50px;" class="rounded-circle">
                                    @{{ privsteMsg.name }}
                                    <p>Mande uma menssagem</p>
                                </li>

                                <li v-else @click="messages(privsteMsg.id)" style="list-style: none;margin-top: 10px;background-color: #ddd;width: 220px;padding: 10px">
                                    <img :src="'img/' + privsteMsg.imagem" style="width:50px;height: 50px;" class="rounded-circle">
                                    @{{ privsteMsg.name }}
                                    <p>Mande uma menssagem</p>
                                </li>
                            </div>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">Menssagens</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div class="row">
                                <div v-for="singleMsg in singleMsgs" class="col-sm-12 col-md-12">
                                    <div v-if="singleMsg.id_De == <?php echo Auth::user()->id; ?>">
                                        <img :src="'img/' + singleMsg.imagem" style="width:30px;height: 30px;float: right;margin:5px" class="rounded-circle ">

                                        <div style="float:right;background-color: #8c98a7;padding: 5px;margin:5px;border-radius: 5px">
                                            @{{ singleMsg.menssagem }}
                                        </div>

                                    </div>
                                    <div v-else>
                                        <img :src="'img/' + singleMsg.imagem" style="width:30px;height: 30px;float: left;margin:5px" class="rounded-circle ">

                                        <div style="float:left;background-color: #2b41a7;padding: 5px;margin:5px;border-radius: 5px;color: #ffffff;">
                                            @{{ singleMsg.menssagem }}
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <br>
                            <div v-if="conID != 0">
                                <form>
                                    @csrf
                                <textarea class="col-md-12 form-control" v-model="msgFrom" @keydown="inputHandler"></textarea>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')

    <script src="{{asset('js/app.js')}}" type="text/javascript"></script>


@endsection

