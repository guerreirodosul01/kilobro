@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        @if(Auth::check())

                            <div class="card bg-white col-md-7  m-auto">
                                <div v-if="!image">
                                    <form method="post" enctype="multipart/form-data" v-on:submit.prevent="addPost">
                                        <div class="post-new-contain row card-block">
                                            <div class="col-md-1 col-xs-3 post-profile">
                                                <img src="img/{{Auth::user()->imagem}}" style="height: 50px" alt="">
                                            </div>

                                            <div class="col-md-11 col-xs-9">
                        <textarea v-model="content" id="post-message" class="form-control post-input" rows="3" cols="10"
                                  placeholder="O que você está pensando ?" required=""></textarea>

                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">
                <span class="image-upload m-r-15" data-toggle="tooltip" data-placement="top" title=""
                      data-original-title="Add Photos">
                                          <label for="file-input" class="file-upload-lbl">
                                            <i class="icofont icofont-image text-muted"></i>
                                          </label>

                                          <input @change="onFileChange" id="file-input" type="file"
                                                 accept="image/x-png,image/gif,image/jpeg">
                                        </span>
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Postar">
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>

                                <div v-else>
                                    <form method="post" enctype="multipart/form-data" v-on:submit.prevent="uploadImg">
                                        <div class="post-new-contain row card-block">
                                            <div class="col-md-1 col-xs-3 post-profile">
                                                <img src="img/{{Auth::user()->imagem}}" style="height: 50px" alt="">
                                            </div>

                                            <div class="col-md-11 col-xs-9">
                        <textarea v-model="content" id="post-message" class="form-control post-input" rows="3" cols="10"
                                  placeholder="O que você está pensando ?" required=""></textarea>

                                            </div>
                                            <div class="col-md-12 col-xs-9">
                                                <img :src="image"
                                                     style="display:block;width:80%; margin:auto;margin-top: 20px;margin-bottom: 20px"/><br>
                                            </div>

                                        </div>
                                        <div class="post-new-footer b-t-muted p-15">

                                            <label for="file-input" class="file-upload-lbl" @click="removeImg">
                                                <i class="icofont icofont-close text-muted"></i>
                                            </label>


                                            </span>
                                            <input type="submit" id="post-new"
                                                   class="btn btn-primary waves-effect waves-light f-right"
                                                   value="Postar">
                                            <!--<button type="submit" class="btn btn-primary f-right">Post</button>-->
                                        </div>
                                    </form>
                                </div>
                            </div>

                        @endif
                        <br>

                        <div v-for="post,key in posts" class="col-sm-12 col-md-12">
                            <div class=" card bg-white p-relative col-md-7  m-auto">
                                @if(Auth::check())
                                    <div class=" wall-elips">
                        <span class="dropdown-toggle addon-btn text-muted f-right wall-dropdown" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false" role="tooltip"></span>
                                        <div class="dropdown-menu dropdown-menu-right b-none services-list"
                                             x-placement="bottom-end"
                                             style="position: absolute; transform: translate3d(550px, 41px, 0px); top: 20px; left: 0px; will-change: transform;">
                                            <li class="dropdown-item" v-if="post.user_id == '{{Auth::user()->id}}'"><a
                                                    data-toggle="modal"  :data-target="'#myModal' + post.id"
                                                    @click="openModal(post.id)">Editar</a></li>
                                            <li class="dropdown-item" v-if="post.user_id == '{{Auth::user()->id}}'">
                                                <a @click="deletePost(post.id)">
                                                    <i class="fa fa-trash"></i> Delete</a>
                                            </li>
                                            <!-- Modal content-->


                                        </div>
                                    </div>
                                    <div class="modal fade" :id="'myModal'+ post.id" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Editar Post</h4>
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        &times;
                                                    </button>

                                                </div>
                                                <div class="modal-body">
                                    <textarea v-model="updatedContent" id="post-message" class="form-control post-input"
                                              rows="3" cols="10"
                                              placeholder="O que você está pensando ?"
                                              required="">@{{post.content}}</textarea>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button"
                                                            class="btn btn-danger waves-effect waves-light f-left"
                                                            data-dismiss="modal">Fechar
                                                    </button>
                                                    <button type="button" id="post-new"
                                                            class="btn btn-primary waves-effect waves-light f-right"
                                                            data-dismiss="modal"
                                                            @click="updatePost(post.id)">Salvar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="card-block">
                                    <div class="media">
                                        <div class="media-left media-middle friend-box">
                                            <a v-bind:href="'profile/' + post.user.slug">
                                                <img class="media-object img-radius m-r-20"
                                                     :src="'img/' + post.user.imagem" alt="">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="chat-header">@{{post.user.name}}</div>
                                            <div class="f-13 text-muted">@{{ post.created_at | myOwnTime }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div id="lightgallery" class="lightgallery-popup">

                                    <img v-if="post.image"
                                         :src="'img/' + post.image" class="img-fluid width-100" alt="">

                                </div>
                                <div class="card-block">
                                    <div class="timeline-details">
                                        <p class="text-muted">@{{post.content}}</p>
                                    </div>
                                </div>
                                <div class="card-block b-b-theme b-t-theme social-msg">
                                    <a style="margin-right: 10px">


                                        @if(Auth::check())
                                            <div v-if="!post.listlikes.includes( {{Auth::user()->id}})">
                                                <i class="icofont icofont-heart-alt text-muted">

                                                </i>
                                                <i
                                                    @click="likePost(post.id)">Like (@{{post.likes.length}})</i>

                                            </div>
                                            <div v-else>
                                                <i class="icofont icofont-heart-alt text-muted">

                                                </i>
                                                <i @click="deslikePost(post.id)">Deslike (@{{post.likes.length}})</i>

                                            </div>
                                        @else
                                            <i class="icofont icofont-heart-alt text-muted">

                                            </i>
                                            <span>Like (@{{post.likes.length}})</span>
                                        @endif
                                    </a>

                                    <i class="icofont icofont-comment text-muted">
                                    </i>
                                    <span onclick="var no = this.parentNode.nextSibling.nextSibling;if (no.style.display === 'none') {
                                        no.style.display = 'block';
                                        } else {
                                        no.style.display = 'none';
                                        }">Comentarios (@{{post.comments.length}})</span>


                                    <!--<img class="media-object img-radius m-r-10" src="../files/assets/images/avatar-1.jpg" alt="">-->
                                    <!--<img class="media-object img-radius m-r-10" src="../files/assets/images/avatar-1.jpg" alt="">-->
                                    <!---->
                                </div>
                                <div class="card-block user-box" style="display: none;" :id="'commentSeen'+key">
                                    <div class="p-b-20">
                                                                        <span class="f-14">Comentarios (@{{post.comments.length}})
                                        </span>

                                    </div>
                                    <div class="media col-md-12" v-for="comment in post.comments">
                                        @if(Auth::check())
                                            <div class="media col-md-12" v-if="comment.user_id=={{Auth::user()->id}}">
                                                <a class="media-left" href="{{url('/profile')}}/{{Auth::user()->slug}}">
                                                    <img class="media-object img-radius m-r-20"
                                                         src="img/{{Auth::user()->imagem}}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body b-b-theme social-client-description">
                                                    <a href="{{url('/profile')}}/{{Auth::user()->slug}}">
                                                        <div class="chat-header">{{Auth::user()->name}}</div>
                                                    </a>
                                                    <p class="text-muted">@{{comment.comment}}</p>
                                                </div>
                                            </div>
                                            <div class="media col-md-12" v-else>
                                                <a class="media-left" :href="'{{url('/profile')}}/' + post.user.slug">
                                                    <img class="media-object img-radius m-r-20"
                                                         :src="'img/' + post.user.imagem"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body b-b-theme social-client-description">
                                                    <div class="chat-header">@{{post.user.name}}</div>
                                                    <p class="text-muted">@{{comment.comment}}</p>
                                                </div>
                                            </div>
                                        @else
                                            <div class="media col-md-12">
                                                <a class="media-left" :href="'{{url('/profile')}}/' + post.user.slug">
                                                    <img class="media-object img-radius m-r-20"
                                                         src="..\files\assets\images\avatar-1.jpg"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="media-body b-b-theme social-client-description">
                                                    <div class="chat-header">@{{post.user.name}}</div>
                                                    <p class="text-muted">@{{comment.comment}}</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    @if(Auth::check())
                                        <div class="media">

                                            <img class="media-object img-radius m-r-20"
                                                 src="img/{{Auth::user()->imagem}}"
                                                 alt="Generic placeholder image">

                                            <div class="media-body">
                                                <form class="">
                                                    <div class="">
                                        <textarea class="f-13 form-control msg-send" rows="3" cols="10" required=""
                                                  placeholder="Comentar....." v-model="commentData[key]"></textarea>
                                                        <div class="text-right m-t-20"><a @click="addComment(post,key)"
                                                                                          class="btn btn-primary waves-effect waves-light">Enviar</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/app.js')}}" type="text/javascript"></script>
@endsection
