@extends('layouts.user')

@section('content')
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->

                    <form class="md-float-material form-material" method="POST" action="/setPass">
                        @csrf
                        <div class="text-center">
                            <h1 class="text-white">Kilobro</h1>
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Esqueceu a senha?</h3>
                                    </div>
                                </div>
                                <input type="hidden" name="token" value="{{ $data[0]->token }}">
                                <div class="form-group form-primary">
                                    <input type="text" name="email" class="form-control" required=""
                                           placeholder="Seu Email"
                                           autocomplete="email" autofocus value="{{ $data[0]->email ?? old('email') }}">
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <label for="password" class=" col-form-label text-md-right">Senha</label>

                                    <div >
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group form-primary">
                                    <label for="password-confirm" class=" col-form-label text-md-right">Confirme a senha</label>

                                    <div >
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit"
                                                class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">
                                            Salvar senha
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-10">
                                        <p class="text-inverse text-left m-b-0">Obrigado.</p>
                                        <p class="text-inverse text-left"><a href="/"><b class="f-w-600">Voltar ao
                                                    site</b></a>
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>

@endsection
