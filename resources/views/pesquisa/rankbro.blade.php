@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">

                                <!-- Tab panes -->

                                <h4 class="m-b-20">Rank das pessoas com maior BroPoints</h4>
                                <div class="row">
                                    <!-- Search result found start -->
                                    <div class="col-lg-12 col-xl-12 search2 search-result">
                                        <div class="card card-main">
                                            <div class="card-block">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <?php $pos = 0 ?>
                                                        @foreach($users as $user)
                                                                <?php $pos = $pos + 1 ?>
                                                            <div class="search-content"
                                                                 style="margin-bottom:10px;margin-top: 10px">
                                                                <h1>{{$pos}}º&nbsp;&nbsp;</h1>
                                                                <img class="card-img-top" src="{{asset('img/'.$user->imagem)}}" style="width: 50px">
                                                                <div class="card-block">
                                                                    <a href="/profile/{{$user->slug}}">
                                                                        <h5 class="card-title">{{$user->name}}

                                                                        </h5>
                                                                    </a>
                                                                    <p class="card-text text-muted"
                                                                       style="max-width: 200ch;overflow: hidden; text-overflow: ellipsis;  white-space: nowrap;"> BroPoints: {{$user->profile->rankpontos}} </p>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Search result found end -->
                                </div>


                            </div>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/pergunta.js')}}" type="text/javascript"></script>
@endsection
