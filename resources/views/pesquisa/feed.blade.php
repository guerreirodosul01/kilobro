@extends('layouts.user')

@section('content')
    <div class="pcoded-content">
        <div class="pcoded-inner-content" style="padding-top: 6%">

            <!-- Main-body start -->
            <div class="main-body">
                <div class="container" id="app">
                    <div>
                        <div class="row">
                            <div class="col-lg-12 col-xl-12">
                                <div class="sub-title">Resultados da pesquisa</div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs  tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">Perguntas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Codigos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#messages1" role="tab">Artigos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#settings1" role="tab">Usuarios</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#trabalhos" role="tab">Trabalhos</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content tabs card-block">
                                    <div class="tab-pane active" id="home1" role="tabpanel">
                                        <h4 class="m-b-20"><b>{{ count($perguntas) }}</b> Resultados encontrados</h4>
                                        <div class="row">
                                            <!-- Search result found start -->
                                            <div class="col-lg-12 col-xl-12 search2 search-result">
                                                <div class="card card-main">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                @foreach($perguntas as $pergunta)
                                                                <div class="search-content" style="margin-bottom:10px;margin-top: 10px">
                                                                    <img class="card-img-top" src="{{asset('img/'.$pergunta->user->imagem)}}" style="width: 50px">
                                                                    <div class="card-block">
                                                                        <a href="pergunta/{{$pergunta->id}}">
                                                                        <h5 class="card-title">{{$pergunta->titulo}}

                                                                        </h5>
                                                                        </a>
                                                                        <p class="card-text text-muted" style="max-width: 200ch;overflow: hidden; text-overflow: ellipsis;  white-space: nowrap;"> {{$pergunta->content}} </p>
                                                                        <p class="card-text"><a href="profile/{{$pergunta->user->slug}}"> {{$pergunta->user->name}}</a> <small class="text-muted">{{$pergunta->updated_at}}</small></p>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Search result found end -->
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="profile1" role="tabpanel">
                                        <h4 class="m-b-20"><b>{{ count($codigos) }}</b> Resultados encontrados</h4>
                                        <div class="row">
                                            <!-- Search result found start -->
                                            <div class="col-lg-12 col-xl-12 search2 search-result">
                                                <div class="card card-main">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                @foreach($codigos as $codigo)
                                                                    <div class="search-content" style="margin-bottom:10px;margin-top: 10px">
                                                                        <img class="card-img-top" src="{{asset('img/'.$codigo->user->imagem)}}" style="width: 50px">
                                                                        <div class="card-block">
                                                                            <a href="infocodigo/{{$codigo->id}}">
                                                                                <h5 class="card-title">{{$codigo->titulo}}

                                                                                </h5>
                                                                            </a>
                                                                            <p class="card-text"><a href="profile/{{$codigo->user->slug}}"> {{$codigo->user->name}}</a> <small class="text-muted">{{$codigo->updated_at}}</small></p>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Search result found end -->
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="messages1" role="tabpanel">
                                        <h4 class="m-b-20"><b>{{ count($artigos) }}</b> Resultados encontrados</h4>
                                        <div class="row">
                                            <!-- Search result found start -->
                                            <div class="col-lg-12 col-xl-12 search2 search-result">
                                                <div class="card card-main">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                @foreach($artigos as $artigo)
                                                                    <div class="search-content" style="margin-bottom:10px;margin-top: 10px">
                                                                        <img class="card-img-top" src="{{asset('img/'.$artigo->user->imagem)}}" style="width: 50px">
                                                                        <div class="card-block">
                                                                            <a href="artigo/{{$artigo->id}}">
                                                                                <h5 class="card-title">{!! $artigo->titulo !!}

                                                                                </h5>
                                                                            </a>
                                                                            <p class="card-text text-muted" style="max-width: 100ch;overflow: hidden; text-overflow: ellipsis;  white-space: nowrap;"> {{$artigo->content}} </p>
                                                                            <p class="card-text"><a href="profile/{{$artigo->user->slug}}"> {{$artigo->user->name}}</a> <small class="text-muted">{{$artigo->updated_at}}</small></p>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Search result found end -->
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="settings1" role="tabpanel">
                                        <h4 class="m-b-20"><b>{{ count($users) }}</b> Resultados encontrados</h4>
                                        <div class="row">
                                            <!-- Search result found start -->
                                            <div class="col-lg-12 col-xl-12 search2 search-result">
                                                <div class="card card-main">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                @foreach($users as $user)
                                                                    <div class="search-content" style=" margin-bottom:10px;margin-top: 10px">
                                                                        <img class="card-img-top" src="{{asset('img/'.$user->imagem)}}" style="width: 50px">
                                                                        <div class="card-block">


                                                                            <a href="/profile/{{$user->slug}}">
                                                                                <h5 class="card-title">{{$user->name}}

                                                                                </h5>
                                                                            </a>
                                                                            <br>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Search result found end -->
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="trabalhos" role="tabpanel">
                                        <h4 class="m-b-20"><b>{{ count($jobs) }}</b> Resultados encontrados</h4>
                                        <div class="row">
                                            <!-- Search result found start -->
                                            <div class="col-lg-12 col-xl-12 search2 search-result">
                                                <div class="card card-main">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <br>
                                                                <br>
                                                                @foreach($jobs as $job)
                                                                    <!-- Job card start -->
                                                                        <div class="card">
                                                                            <div class="card-header">
                                                                                <div class="media">
                                                                                    <a class="media-left media-middle" href="/profile/{{$job->slug}}">
                                                                                        <img class="media-object img-60" src="{{asset('img/'.$job->imagem)}}" alt="Generic placeholder image">
                                                                                    </a>
                                                                                    <div class="media-body media-middle">
                                                                                        <div class="company-name">
                                                                                            <p>{{$job->job_title}}</p>
                                                                                            <span class="text-muted f-14">{{$job->created_at}}</span>
                                                                                        </div>
                                                                                        <div class="job-badge">
                                                                                            <label class="label bg-primary">Trabalho</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="card-block">
                                                                                <p class="text-muted">{{$job->requirements}}</p>
                                                                                <br>
                                                                                <br>
                                                                                <br>
                                                                                @if($job->skills != " ")
                                                                                <div class="job-lable">
                                                                                    <label class="label badge-success">{{$job->skills}}</label>
                                                                                </div>
                                                                                @endif
                                                                                <h4>Email para mandar curriculo: <a href="mailto:{{$job->email}}" style="font-size: 30px;">{{$job->email}}</a></h4>

                                                                            </div>
                                                                        </div>
                                                                        <!-- Job card end -->
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Search result found end -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/pergunta.js')}}" type="text/javascript"></script>
@endsection
