<?php
date_default_timezone_set('America/Sao_Paulo');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Pergunta;
use App\post;
use Illuminate\Support\Facades\DB;



//Esqueceu a senha
Route::get('/forgotPassword', function () {
    return view('profile.forgotPassword');
});
Route::post('/setToken', 'ProfileController@setToken');
Route::get('/getToken/{token}', function ($token) {
    if (isset($token) && $token != "") {
        $getData = \Illuminate\Support\Facades\DB::table('password_resets')->where('token', '=', $token)->get();
        if (count($getData) != 0) {
            return view('auth.passwords.reset')->with('data', $getData);
        } else {
            echo "token is wrong";
        }
    } else {
        echo "token is wrong";
    }

});
Route::post('/setPass', 'ProfileController@setPass');
//fim esqueceu a senha


Auth::routes();

//Post Feed
Route::group([], function () {
    Route::get('/', 'PostsController@index');
    Route::get('/posts', 'PostsController@posts');
});
//Post Perfil
Route::group([], function () {
    Route::post('perfil/posts', 'ProfileController@posts');
});


//Post Pergunta
Route::group(['prefix' => 'pergunta'], function () {
    Route::get('/{id}', 'PerguntaController@pergunta');
    Route::get('infopergunta/{id}', 'PerguntaController@infoperunta');
});
//fim post Pergunta

//Post Artigo
Route::group(['prefix' => 'artigo'], function () {
    Route::get('/{id}', 'ArtigoController@pergunta');
    Route::get('infoartigo/{id}', 'ArtigoController@infoartigo');

});
//fim post artigo

Route::group(['middleware' => 'auth'], function () {
    //chat
    Route::get('/getConversas', 'ConversasController@getConversas');
    Route::get('/getConversas/{id}', 'ConversasController@getConversasId');
    Route::post('/sendMessage', 'ConversasController@sendMessage');
    Route::get('/menssagens', function () {
        return view('menssagens');
    });


    //Post Feed
    Route::group([], function () {
        Route::post('/addPost', 'PostsController@addPost');
        Route::get('/deletePost/{id}', 'PostsController@deletePost');
        Route::get('/likePost/{id}', 'PostsController@likePost');
        Route::get('/deslikePost/{id}', 'PostsController@deslikePost');
        Route::post('addComment', 'PostsController@addComment');
        Route::post('saveImg', 'PostsController@saveImg');
        Route::post('updatePost/{id}', 'PostsController@updatePost');
        Route::get('posts/{id}', function ($id) {
            $pData = App\post::where('id', $id)->get();
            echo $pData[0]->content;
        });
    });
    //fim post feed

    //Post Perfil
    Route::group(['prefix' => 'perfil'], function () {
        Route::post('/addPost', 'ProfileController@addPost');
        Route::get('/deletePost/{id}', 'ProfileController@deletePost');
        Route::post('/likePost', 'ProfileController@likePost');
        Route::post('/deslikePost', 'ProfileController@deslikePost');
        Route::post('addComment', 'ProfileController@addComment');
        Route::post('saveImg', 'ProfileController@saveImg');
        Route::post('updatePost/{id}', 'ProfileController@updatePost');
        Route::get('posts/{id}', function ($id) {
            $pData = App\post::where('id', $id)->get();
            echo $pData[0]->content;
        });
    });
    //fim post Perfil


    //Post Pergunta
    Route::group(['prefix' => 'pergunta'], function () {
        Route::post('/addPost', 'PerguntaController@addPost');
        Route::get('/deletePost/{id}', 'PerguntaController@deletePost');
        Route::get('/likePost/{id}', 'PerguntaController@likePost');
        Route::get('/deslikePost/{id}', 'PerguntaController@deslikePost');
        Route::post('addComment', 'PerguntaController@addComment');
        Route::post('saveImg', 'PerguntaController@saveImg');
        Route::post('updatePost/{id}', 'PerguntaController@updatePost');
        Route::get('posts/{id}', function ($id) {
            $pData = App\Pergunta::where('id', $id)->get();
            echo $pData;
        });
    });
    //fim post Pergunta


    //Post Artigo
    Route::group(['prefix' => 'artigo'], function () {
        Route::post('/addPost', 'ArtigoController@addPost');
        Route::get('/deletePost/{id}', 'ArtigoController@deletePost');
        Route::get('/likePost/{id}', 'ArtigoController@likePost');
        Route::get('/deslikePost/{id}', 'ArtigoController@deslikePost');
        Route::post('addComment', 'ArtigoController@addComment');

    });
    //fim post artigo


    //perfil
    Route::get('/desfazerAmizade/{id}', 'ProfileController@desfazerAmizade');
    Route::post('uploadFoto', 'ProfileController@uploadFoto');


    Route::get('/editProfile', 'ProfileController@editProfile');
    Route::post('/updateProfile', 'ProfileController@updateProfile');
    Route::get('/addFriend/{id}', 'ProfileController@addFriend');
    Route::get('/pedidos', 'ProfileController@pedidos')->name('pedidos');
    Route::get('/acceptFriend/{name}/{id}', 'ProfileController@acceptFriend');
    Route::get('/recusarFriend/{id}', 'ProfileController@recusarFriend');
    Route::get('/notificacoes/{id}', 'ProfileController@notificacoes');

    //jobs for users
    Route::get('jobs', 'ProfileController@jobs');
    Route::get('job/{id}', 'ProfileController@job');


    //perguntas
    Route::get('/fazerpergunta', 'PerguntaController@criar')->name('fazerpergunta');

    //artigos
    Route::get('/fazerartigo', 'ArtigoController@criar')->name('fazerartigo');
    Route::post('/adicionaArtigo', 'ArtigoController@novoArtigo')->name('adicionaArtigo');

    //codigo
    Route::get('/fazercodigo', 'CodigoController@criar')->name('fazercodigo');
    Route::post('/adicionacodigo', 'CodigoController@novoCodigo')->name('adicionacodigo');

    //trabalhos
    Route::get('/addJob', function () {
        return view('company.addJob');
    })->name('criartrabalho');

    Route::post('/addJobSubmit', 'companyController@addJobSubmit');


});

Route::get('pesquisa', 'PesquisaController@pesquisa')->name('pesquisa');
Route::get('rankbro', 'PesquisaController@rankbro')->name('rankbro');
Route::get('/profile/{slug}', 'ProfileController@index');
Route::get('infocodigo/{id}', 'CodigoController@infocodigo');
Route::get('company/jobs', 'companyController@viewJobs');



Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'adminController@index');

});

